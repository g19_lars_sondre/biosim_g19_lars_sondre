The goal for this project is to simulate a population of herbivores and carnivores on an island. 
Members of the team: Lars-Petter Andersen and John Sondre Sikkeland

11.01.2019 14:04 : First simulation of herbivores in a jungle successful! Hooray!
16.01.2019 13.10 : First fully funtional herbivore and carnivore in a single cell! Woohoo!
17.01.2019 11:02 : First functional simulation of herbivore and carnivore migration for one year, within the residebable landscape types!
17.01.2019 15:53 : Small fully working simulation of herbivore and carnivore migration for several years, in a small map
19.01.2019 11.46 : Working simulation of the entired Island, and a plot of distribution of the herbivores vs the carnivores.
21.01.2019 09.22 : Included a map the island with color codes in the simulation.
21.01.2019 12.26 : Added a visualization of the carnivore and herbivore distribution on the island.
21.01.2019 18.10 : Added the sliders and button to the visualization. 


