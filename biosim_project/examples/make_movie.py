# -*- coding: utf-8 -*-

"""
Small test script to show how movies can be created
"""

__author__ = 'John Sondre Sikkeland, Lars-Petter Andersen'
__email__ = 'johnsikk@nmbu.no, larsand@nmbu.no'


import matplotlib.pyplot as plt
from biosim.simulation import BioSim

if __name__ == "__main__":
    plt.ion()
    ini_herbs = [{'loc': (2, 2),
                  'pop': [{'species': 'Herbivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(10)]}]
    ini_carns = [{'loc': (2, 3),
                  'pop': [{'species': 'Carnivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(10)]}]

    sim = BioSim(island_map="OOOO\nOJJO\nOOOO",
                 ini_pop=ini_herbs,
                 seed=12345,
                 img_base=".\\simulation")  # files will be formed as simulation00000.png and so on
    sim.simulate(num_years=10, vis_years=1)
    sim.add_population(population=ini_carns)
    sim.simulate(num_years=10, vis_years=1)
    sim.make_movie()
