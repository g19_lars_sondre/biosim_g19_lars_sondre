# -*- coding: utf-8 -*-

import textwrap
from biosim.simulation import BioSim


"""
Self check for the migration. With 100% move probability animals should always alternate between 
cells, like a checkerboard. This example shows that this work.
"""

__author__ = 'John Sondre Sikkeland, Lars-Petter Andersen'
__email__ = 'johnsikk@nmbu.no, larsand@nmbu.no'


if __name__ == '__main__':
    geogr = """\
               OOOOOOOOO
               ODDDDDDDO
               ODDDDDDDO
               ODDDDDDDO
               ODDDDDDDO
               ODDDDDDDO
               ODDDDDDDO
               ODDDDDDDO
               OOOOOOOOO"""
    geogr = textwrap.dedent(geogr)

    ini_herbs = [{'loc': (5, 5),
                  'pop': [{'species': 'Herbivore',
                           'age': 0,
                           'weight': 100}
                          for _ in range(1000)]}]

    ini_carns = [{'loc': (5, 5),
                  'pop': [{'species': 'Carnivore',
                           'age': 0,
                           'weight': 100}
                          for _ in range(1000)]}]

    sim = BioSim(island_map=geogr, ini_pop=ini_herbs,
                 seed=123456)

    sim.set_animal_parameters('Herbivore',
                              {'mu': 1, 'omega': 0, 'gamma': 0,
                               'a_half': 1000})
    sim.set_animal_parameters('Carnivore',
                              {'mu': 1, 'omega': 0, 'gamma': 0,
                               'F': 0, 'a_half': 1000})
    sim.add_population(population=ini_carns)
    sim.simulate(num_years=100, vis_years=1, img_years=10000)
