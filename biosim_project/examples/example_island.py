# -*- coding: utf-8 -*-

import textwrap
import matplotlib.pyplot as plt

from biosim.simulation import BioSim

"""
Check that the example island provided by EPAP gives about same results as shown in lecture 5, 
14 Jan 19. 
"""

__author__ = 'John Sondre Sikkeland, Lars-Petter Andersen'
__email__ = 'johnsikk@nmbu.no, larsand@nmbu.no'


if __name__ == '__main__':
    plt.ion()

    geogr = """\
               OOOOOOOOOOOOOOOOOOOOO
               OSSSSSJJJJMMJJJJJJJOO
               OSSSSSJJJJMMJJJJJJJOO
               OOSSJJJJJJJMMJJJJJJJO
               OOSSJJJJJJJMMJJJJJJJO
               OOOOOOOOSMMMMJJJJJJJO
               OSSSSSJJJJMMJJJJJJJOO
               OSSSSSSSSSMMJJJJJJOOO
               OSSSSSDDDDDJJJJJJJOOO
               OSSSSSDDDDDJJJJJJJOOO
               OSSSSSDDDDDJJJJJJJOOO
               OSSSSSDDDDDMMJJJJJOOO
               OSSSSDDDDDDJJJJOOOOOO
               OOSSSSDDDDDDJOOOOOOOO
               OOSSSSDDDDDJJJOOOOOOO
               OSSSSSDDDDDJJJJJJJOOO
               OSSSSDDDDDDJJJJOOOOOO
               OOSSSSDDDDDJJJOOOOOOO
               OOOSSSSJJJJJJJOOOOOOO
               OOOSSSSSSOOOOOOOOOOOO
               OOOOOOOOOOOOOOOOOOOOO
               """
    geogr = textwrap.dedent(geogr)

    ini_herbs = [{'loc': (2, 7),
                  'pop': [{'species': 'Herbivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(200)]}]
    ini_carns = [{'loc': (2, 7),
                  'pop': [{'species': 'Carnivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(50)]}]

    sim = BioSim(island_map=geogr, ini_pop=ini_herbs,
                 seed=123456, img_base=".\exampleisland")
    sim.add_population(ini_carns)
    sim.simulate(num_years=401, vis_years=1, img_years=50)
