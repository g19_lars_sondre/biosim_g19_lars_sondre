# -*- coding: utf-8 -*-

from biosim.simulation import BioSim
import random

"""
Self check. 
50 herbivores with random age and weight placed at start
Simulated 50 years 5 carnivores with random age and weight placed
Simulate 250 years
Simulations with five different seeds
"""

__author__ = 'John Sondre Sikkeland, Lars-Petter Andersen'
__email__ = 'johnsikk@nmbu.no, larsand@nmbu.no'


if __name__ == '__main__':
    ini_herbs = [{'loc': (2, 2),
                  'pop': [{'species': 'Herbivore',
                           'age': random.randint(0, 20),
                           'weight': random.randint(1, 60)}
                          for _ in range(50)]}]
    ini_carns = [{'loc': (2, 2),
                  'pop': [{'species': 'Carnivore',
                           'age': random.randint(0, 20),
                           'weight': random.randint(1, 60)}
                          for _ in range(5)]}]
    sim = BioSim(island_map="OOO\nOJO\nOOO", ini_pop=ini_herbs,
                 seed=123456)
    sim.simulate(num_years=50)
    sim.add_population(ini_carns)
    sim.simulate(num_years=250)

