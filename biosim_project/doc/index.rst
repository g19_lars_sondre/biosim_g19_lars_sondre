.. BioSim January Project 2019 documentation master file, created by
   sphinx-quickstart on Wed Jan 23 09:44:38 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BioSim January Project 2019's documentation!
=======================================================

This is a simulation of animals on the Island Rossumøya. The simulation implements animal types
Herbivores and Carnivores. 5 different landscape types are implemented:
 * Ocean
 * Mountain
 * Jungle
 * Savannah
 * Desert

The simulation module can simulate population dynamics on the Island.

.. toctree::
   :maxdepth: 2

   sim_and_island
   landscape_and_animals


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
