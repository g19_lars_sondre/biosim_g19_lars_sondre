Landscape and animals
=====================

The landscape module
--------------------
.. automodule:: biosim.landscape
   :members:

The animals module
-------------------
.. automodule:: biosim.animals
   :members:

