# -*- coding: utf-8 -*-

"""
Tests for the island class in the island.py module
"""

__author__ = 'John Sondre Sikkeland, Lars-Petter Andersen'
__email__ = 'johnsikk@nmbu.no, larsand@nmbu.no'

from biosim.island import Island
from biosim.landscape import *
import pytest


def test_empty_island():
    """Empty island can be created"""
    i = Island(island_map="OO\nOO")
    for j in range(2):
        for k in range(2):
           assert isinstance(i.geography[j][k], Ocean)


def test_minimal_island():
    """Island of single jungle cell"""
    i = Island(island_map="OOO\nOJO\nOOO")
    assert isinstance(i.geography[0][0], Ocean)
    assert isinstance(i.geography[1][1], Jungle)


def test_inconsistent_length():
    """Inconsistent line length must raise error"""
    with pytest.raises(ValueError):
        Island(island_map="OOO\nOJJO\nOOO")


def test_all_types():
    """All types of landscape in the landscape module can be created"""
    i = Island(island_map="OOOO\nOJSO\nOMDO\nOOOO")
    assert isinstance(i.geography[0][0], Ocean)
    assert isinstance(i.geography[1][1], Jungle)
    assert isinstance(i.geography[1][2], Savannah)
    assert isinstance(i.geography[2][1], Mountain)
    assert isinstance(i.geography[2][2], Desert)


@pytest.mark.parametrize('bad_boundary',
                         ['J', 'S', 'M', 'D'])
def test_invalid_top_left_boundary(bad_boundary):
    """Non-ocean boundary must raise error"""
    with pytest.raises(ValueError):
        Island(island_map="{}OO\nOJO\nOOO".format(bad_boundary))


@pytest.mark.parametrize('bad_boundary',
                         ['J', 'S', 'M', 'D'])
def test_invalid_top_boundary(bad_boundary):
    """Invalid boundary must raise error"""
    with pytest.raises(ValueError):
        Island(island_map="O{}O\nOJO\nOOO".format(bad_boundary))


def test_invalid_landscape():
    """Invalid landscape type must raise error"""
    with pytest.raises(ValueError):
        Island(island_map="OOO\nORO\nOOO")


def test_island_add_herbivores():
    """Test that herbivores can be placed on the island"""
    ini_herbs = [{'loc': (2, 2),
                  'pop': [{'species': 'Herbivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(50)]}]
    i = Island(island_map="OOO\nOJO\nOOO", ini_pop=ini_herbs)
    assert len(i.geography[1][1].h_pop) == 50


def test_island_improper_placement_value_error():
    """Test that a ValueError occurs when trying to place animals on inhabitable land"""
    ini_herbs = [{'loc': (0, 0),
                  'pop': [{'species': 'Herbivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(50)]}]
    with pytest.raises(ValueError):
        Island(island_map="OOO\nOJO\nOOO", ini_pop=ini_herbs)


def test_island_can_add_animals():
    """ Test that animals can be placed separately on island"""
    i = Island(island_map="OOO\nOJO\nOOO")
    herbs = [{'loc': (2, 2),
              'pop': [{'species': 'Herbivore',
                       'age': 5,
                       'weight': 20}
                      for _ in range(50)]}]
    i.add_population(population=herbs)
    assert len(i.geography[1][1].h_pop) == 50


def test_island_raises_error_unknown_animal():
    """ Test that unknown animals can not be placed on island"""
    i = Island(island_map="OOO\nOJO\nOOO")
    fish = [{'loc': (2, 2),
             'pop': [{'species': 'Fish',
                      'age': 5,
                      'weight': 20} for _ in range(50)]}]
    with pytest.raises(ValueError):
        i.add_population(population=fish)


def test_landscape_move_list_empty_after_one_year():
    """Test that no landscape tile contains animals in move lists after a year has passed"""
    herbs = [{'loc': (2, 2),
              'pop': [{'species': 'Herbivore',
                       'age': 5,
                       'weight': 20}
                      for _ in range(50)]}]
    i = Island(island_map="OOO\nOJO\nOOO", ini_pop=herbs)
    i.one_year()
    tile = i.geography[1][1]  # The jungle
    assert not tile.move_north
    assert not tile.move_east
    assert not tile.move_south
    assert not tile.move_west


@pytest.fixture
def set_params(request):
    Herbivore.set_params(request.param)
    yield
    Herbivore.set_params(Herbivore.default_params)


@pytest.mark.parametrize('set_params', [{'mu': 1,
                                         'w_half': 0.01,
                                         'gamma': 0}], indirect=True)
def test_all_animals_move(set_params):
    """
    Testing that the animals move
    :param set_params: Setting parameter values.
    """
    ini_herbs = 50
    herbs = [{'loc': (3, 3),
              'pop': [{'species': 'Herbivore',
                       'age': 0,
                       'weight': 100}
                      for _ in range(ini_herbs)]}]
    i = Island(island_map="OOOOO\nOJJJO\nOJJJO\nOJJJO\nOOOOO", ini_pop=herbs)
    i.one_year()
    assert not i.geography[2][2].h_pop
    i.one_year()
    assert len(i.geography[2][2].h_pop) > 0


def test_animals_count_method():
    """ Testing that islands method to count all animals works"""
    ini_herbs = 50
    herbs = [{'loc': (2, 2),
              'pop': [{'species': 'Herbivore',
                       'age': 5,
                       'weight': 20}
                      for _ in range(ini_herbs)]}]
    i = Island(island_map="OOOO\nOJJO\nOOOO", ini_pop=herbs)
    num_herbs, num_carnivores = i.animal_counts()
    assert num_herbs == ini_herbs
    assert num_carnivores == 0
    ini_carnivores = 30
    carnivores = [{'loc': (2, 3),
                   'pop': [{'species': 'Carnivore',
                            'age': 5,
                            'weight': 20}
                           for _ in range(ini_carnivores)]}]
    i.add_population(population=carnivores)
    num_herbs, num_carnivores = i.animal_counts()
    assert num_herbs == ini_herbs
    assert num_carnivores == ini_carnivores


def test_herbivore_stationary_weight():
    """
    Placing 50 animals on a jungle cell and checking that the stationary weight becomes around
    171 as discussed in the lecture given by H.E.Plesser on January 14th, 2019
    """
    ini_herbs = [{'loc': (2, 2),
                  'pop': [{'species': 'Herbivore',
                           'age': 0,
                           'weight':20}
                          for _ in range(50)]}]
    i = Island(island_map="OOO\nOJO\nOOO", ini_pop=ini_herbs)
    Herbivore.set_params({'omega': 0,
                          'gamma': 0})
    for _ in range(500):
        i.one_year()
    assert i.geography[1][1].nh == 50  # Still 50 animals on tile
    assert i.geography[1][1].h_pop[0].w == pytest.approx(171)
