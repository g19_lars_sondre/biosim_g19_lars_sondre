# -*- coding: utf-8 -*-

"""
Tests for the animal classes
"""

__author__ = 'John Sondre Sikkeland, Lars-Petter Andersen'
__email__ = 'johnsikk@nmbu.no, larsand@nmbu.no'


from biosim.animals import *
import pytest


def test_animal_subclasses():
    """ Testing that herbivore and carnivore are subclasses of animal"""
    assert issubclass(Herbivore, Animal)
    assert issubclass(Carnivore, Animal)


class TestAnimals:
    """ Several unit tests for the herbivore and carnivore class"""
    def test_herbivore_constructor(self):
        """Testing whether a carnivore can be created"""
        h = Herbivore()
        assert isinstance(h, Herbivore)
        assert h.a == 0
        herb = Herbivore(age=2)
        assert herb.a == 2

    def test_carnivore_constructor(self):
        """Testing whether a carnivore can be created"""
        c = Carnivore()
        assert isinstance(c, Carnivore)
        assert c.a == 0
        carn = Carnivore(age=2)
        assert carn.a == 2

    def test_animal_properties(self):
        """Testing that animals has the attributes age, weight and fitness"""
        for animal in [Herbivore(), Carnivore()]:
            animal.a
            animal.w
            animal.phi
            with pytest.raises(AttributeError):
                animal.n

    def test_animal_given_weight(self):
        """Testing instance weight is correct when specifying weight"""
        ini_weight = 10
        for animal in [Herbivore(weight=ini_weight), Carnivore(weight=ini_weight)]:
            assert animal.w == ini_weight

    def test_animal_zero_weight(self):
        """ Testing that an animal with weight = 0 also gets fitness = 0 and die"""
        zero = 0
        for animal in [Herbivore(weight=zero), Carnivore(weight=zero)]:
            assert animal.phi == zero
            assert animal.die()

    def test_animal_params(self):
        """Testing that get_params works for both animal types"""
        for animal in [Herbivore(), Carnivore()]:
            assert type(animal.get_params()) is dict
            assert 'w_birth' in animal.get_params()
            assert 'sigma_birth' in animal.get_params()
            assert 'beta' in animal.get_params()
            assert 'eta' in animal.get_params()
            assert 'a_half' in animal.get_params()
            assert 'phi_age' in animal.get_params()
            assert 'w_half' in animal.get_params()
            assert 'phi_weight' in animal.get_params()
            assert 'mu' in animal.get_params()
            assert 'gamma' in animal.get_params()
            assert 'zeta' in animal.get_params()
            assert 'xi' in animal.get_params()
            assert 'omega' in animal.get_params()
            assert 'F' in animal.get_params()

    def test_carnivore_specific_parameter(self):
        """Testing that the carnivore-specific DeltaPhiMax parameter is returned when running
        get_params"""
        assert 'DeltaPhiMax' in Carnivore().get_params()

    def test_animal_unknown_parameter(self):
        """Testing that trying to edit a unknown parameter raises KeyError"""
        with pytest.raises(KeyError):
            Herbivore().set_params({'fodder': 10})
        with pytest.raises(KeyError):
            Carnivore().set_params({'fodder': 10})

    def test_animal_aging(self):
        """ Testing the age method for both animals. Each time the .age() method is called,
        the age should increase by 1"""
        for animal in [Herbivore(), Carnivore()]:
            for n in range(10):
                animal.age_and_lose_weight()
                assert animal.a == n + 1

    def test_animal_looses_weight(self):
        """Testing that the herbivores and carnivores looses weight when entering a new year"""
        for a in [Herbivore(), Carnivore()]:
            for _ in range(5):
                ini_weight = a.w
                a.age_and_lose_weight()
                assert a.w < ini_weight

    def test_herbivore_eating(self):
        """ Testing the Herbivore.eat() method. When animals eat, its weight should increase. The
        available food should decrease"""
        h = Herbivore()
        ini_weight = h.w
        initial_food = 20
        f_available = h.eat(initial_food)
        assert h.w > ini_weight  # Weight after eating is greater than before eating
        assert f_available < initial_food  # Available food decreases after animal has eaten

    def test_animal_die(self, mocker):
        """ Testing animal.die method """
        mocker.patch('random.random', return_value=0.0)
        for a in [Herbivore(), Carnivore()]:
            assert a.die()

    def test_herbivore_can_give_birth(self, mocker):
        """ Testing that Herbivore.give_birth() method works correctly"""
        mocker.patch('random.random', return_value=0)
        ini_weight = 50
        h = Herbivore(weight=ini_weight)
        ini_fitness = h.phi
        assert isinstance(h.give_birth(2), Herbivore)
        assert h.w < ini_weight
        assert h.phi < ini_fitness

    def test_carnivore_can_give_birth(self, mocker):
        """ Testing that the Carnivore_give_birth() method works correctly"""
        mocker.patch('random.random', return_value=0)
        ini_weight = 50
        c = Carnivore(weight=ini_weight)
        ini_fitness = c.phi
        assert isinstance(c.give_birth(2), Carnivore)
        assert c.w < ini_weight
        assert c.phi < ini_fitness

    def test_herbivore_with_low_weight_does_not_give_birth(self, mocker):
        """ Test that Herbivore with weight below zeta*(w_birth+sigma_birth) does not make baby"""
        mocker.patch('random.random', return_value=0)
        limit = Herbivore().zeta * (Herbivore().w_birth + Herbivore().sigma_birth)
        h = Herbivore(weight=limit - 1)  # Create a herbivore with just too little weight
        assert h.give_birth(100) is None
        h.eat(10)  # After eating the herbivore should now have enough weight to make baby
        assert isinstance(h.give_birth(200), Herbivore)

    @pytest.fixture
    def set_params_herb(self, request):
        """
        Fixture setting class parameters on Herbivores.

        The fixture sets Herbivore parameters when they are called for setup,
        and resets them when called for teardown. This ensures that modified
        parameters are always reset before leaving a test.

        This fixture should be called via parametrize with indirect=True.

        Based on https://stackoverflow.com/a/33879151

        Parameters
        ----------
        request
            Request object automatically is provided by pytest.
            request.param is the parameter dictionary to be passed to
            Herbivore.set_params()
        """
        Herbivore.set_params(request.param)
        yield
        Herbivore.set_params(Herbivore.default_params)

    @pytest.mark.parametrize('set_params_herb', [{'w_birth': 0.1}], indirect=True)
    def test_herbivores_have_positive_weight(self, set_params_herb):
        """ No animals should be created (or born) with negative weight"""
        we = [Herbivore().w for _ in range(100)]  # Collecting the weight of 100 newborn
        # Herbivores. Note: The w_birth parameter is set very low,
        for positive_number in we:
            assert positive_number > 0

    @pytest.mark.parametrize('set_params_herb', [{'omega': 0.0}], indirect=True)
    def test_herbivore_certain_survival(self, set_params_herb):
        """ Testing that the animals will survive when omega is set to zero. """
        h = Herbivore()
        for _ in range(100):
            assert not h.die()

    @pytest.mark.parametrize('set_params_herb', [{'omega': 1.0, 'a_half': 0.01,
                                             'w_half': 1000}], indirect=True)
    def test_herbivore_certain_death(self, set_params_herb):
        """ Tests that the animals must die when omega is set to 1"""
        h = Herbivore()
        for __ in range(100):
            assert h.die()

    @pytest.mark.parametrize('set_params_herb',
                             ['w_birth',
                              'sigma_birth',
                              'beta',
                              'eta',
                              'a_half',
                              'phi_age',
                              'w_half',
                              'phi_weight',
                              'mu',
                              'gamma',
                              'zeta',
                              'xi',
                              'omega',
                              'F'])
    def test_herbivore_set_bad_parameters(self, set_params_herb):
        """Testing that setting bad parameters on Herbivores raises ValueError"""
        with pytest.raises(ValueError):
            Herbivore.set_params(new_params={"{}".format(set_params_herb): -1.0})

    def test_herbivore_parameters(self):
        """Testing that the Herbivores has all the parameters, and that they can be changed"""
        h = Herbivore()
        new_params = {'w_birth': 20,
                      'beta': 0.8,
                      'zeta': 3.2,
                      'xi': 1.8}
        Herbivore.set_params(new_params)
        assert h.w_birth == new_params['w_birth']
        assert h.beta == new_params['beta']
        assert h.zeta == new_params['zeta']
        assert h.xi == new_params['xi']

    def test_herbivore_fitness(self):
        """ Testing that the fitness is not less than zero or grater than one"""
        for a in [Herbivore(), Carnivore()]:
            assert a.phi >= 0
            assert a.phi <= 1

    def test_animal_migration(self):
        """ Testing that the animal types return what we want it to do"""
        for _ in range(30):
            for animal in [Herbivore(), Carnivore()]:
                direction = animal.migrate()
                assert direction in (None, 1, 2, 3, 4)

    def test_animal_migration_mocker(self, mocker):
        """ Testing that the method returns what we want it to do, if we know the animal will try
        to move"""
        for _ in range(30):
            for animal in [Herbivore(), Carnivore()]:
                mocker.patch('random.random', return_value=0)  # Making sure animals will always move
                direction = animal.migrate()
                assert direction in (1, 2, 3, 4)  # Since we are absolutely sure it will want to move


class TestCarnivore:
    """¨Several tests for the carnivore class"""

    def test_carnivore_params(self):
        """Testing that get_params works"""
        assert type(Carnivore.get_params()) is dict
        assert 'w_birth' in Carnivore.get_params()
        assert 'F' in Carnivore.get_params()

    @pytest.fixture
    def set_params_carn(self, request):
        """
        Fixture setting class parameters on Carnivores.

        The fixture sets Carnivore parameters when they are called for setup,
        and resets them when called for teardown. This ensures that modified
        parameters are always reset before leaving a test.

        This fixture should be called via parametrize with indirect=True.

        Based on https://stackoverflow.com/a/33879151

        Parameters
        ----------
        request
            Request object automatically is provided by pytest.
            request.param is the parameter dictionary to be passed to
            Herbivore.set_params()
        """
        Carnivore.set_params(request.param)
        yield
        Carnivore.set_params(Carnivore.default_params)

    @pytest.mark.parametrize('set_params_carn', [{'w_birth': 0.1}], indirect=True)
    def test_carnivores_have_positive_weight(self, set_params_carn):
        """ No carnivores should be created (or born) with negative weight"""
        we = [Carnivore().w for _ in range(100)]  # Collecting the weight of 100 newborn
        # Herbivores. Note: The w_birth parameter is set very low,
        for positive_number in we:
            assert positive_number > 0

    @pytest.mark.parametrize('set_params_carn', [{'omega': 0.0}], indirect=True)
    def test_carnivore_certain_survival(self, set_params_carn):
        """ Testing that carnivores must live with a specific parameter"""
        c = Carnivore()
        for _ in range(100):
            assert not c.die()

    @pytest.mark.parametrize('set_params_carn', [{'omega': 1.0, 'a_half': 0.01,
                                                  'w_half': 1000}], indirect=True)
    def test_carnivore_certain_death(self, set_params_carn):
        """ Tests that the herbivore must die when omega is set to 1"""
        c = Carnivore()
        for __ in range(100):
            assert c.die()

    @pytest.mark.parametrize('set_params_carn', [{'DeltaPhiMax': 0.1}], indirect=True)
    def test_carnivore_certain_kill(self, set_params_carn):
        """ Testing that a low DeltaPhiMax gives a certain kill"""
        h_list = [Herbivore()]
        c = Carnivore(weight=20)
        c.eat(h_list)
        assert len(h_list) == 0

    @pytest.mark.parametrize('set_params_carn',
                             ['w_birth',
                              'sigma_birth',
                              'beta',
                              'eta',
                              'a_half',
                              'phi_age',
                              'w_half',
                              'phi_weight',
                              'mu',
                              'gamma',
                              'zeta',
                              'xi',
                              'omega',
                              'F',
                              'DeltaPhiMax'])
    def test_herbivore_set_bad_parameters(self, set_params_carn):
        """Testing that setting bad parameters on Herbivores raises ValueError"""
        with pytest.raises(ValueError):
            Carnivore.set_params(new_params={"{}".format(set_params_carn): -1.0})


    def test_carnivore_parameters(self):
        """Testing whether the Herbivores has all the parameters, and that they can be changed"""
        c = Carnivore()
        new_params = {'w_birth': 20,
                      'beta': 0.8,
                      'zeta': 3.2,
                      'xi': 1.8}
        Carnivore.set_params(new_params)
        assert c.w_birth == new_params['w_birth']
        assert c.beta == new_params['beta']
        assert c.zeta == new_params['zeta']
        assert c.xi == new_params['xi']


class TestAnimalIntegration:
    """ Checking that the animals are compatible - more specific that the
    'carnivore-eats-herbivore'-part behaves as it should"""
    def test_carnivore_hunt(self):
        """ Test for the carnivores eat function"""
        ini_herbivores = 200
        h_list = [Herbivore() for _ in range(ini_herbivores)]  # Herbivores with phi around 0.4-0.5
        h_list.sort(key=lambda x: x.phi, reverse=True)
        c_weight = 30
        c = Carnivore(weight=c_weight)  # Will have fitness around 1 with standard parameters
        c_fitness = c.phi
        c.eat(h_list)
        assert len(h_list) < ini_herbivores
        assert c.w > c_weight
        assert c.phi > c_fitness  # Check that fitness is updated (should be higher after eating)

    def test_carnivore_given_empty_list_of_prey(self):
        """Checking that empty list of prey don't destroy everything"""
        c = Carnivore()
        ini_fitness = c.phi
        c.eat(sorted_herbivores=[])
        assert c.phi == ini_fitness

    def test_low_fitness_carnivore(self):
        """Making sure a carnivore with low fitness wont kill"""
        ini_herbivores = 10
        h_list = [Herbivore() for _ in range(ini_herbivores)]  # Herbivores with phi around 0.4-0.5
        h_list.sort(key=lambda x: x.phi, reverse=True)
        c = Carnivore(age=100, weight=5)
        c.eat(h_list)
        assert len(h_list) == ini_herbivores

    def test_carnivore_without_hunger(self):
        """ Testing that a carnivore without hunger will not kill a herbivore"""
        Carnivore().set_params({'F': 0})
        ini_herbivores = 100
        h_list = [Herbivore() for _ in range(ini_herbivores)]  # Herbivores with phi around 0.4-0.5
        h_list.sort(key=lambda x: x.phi, reverse=True)
        c = Carnivore()
        c.eat(h_list)
        assert len(h_list) == ini_herbivores  # Carnivore without hunger should not kill herbivores

    def test_carnivore_leaves_food(self, mocker):
        """ Testing that a carnivore will leave the cadaver when it is finished eating"""
        mocker.patch('random.random', return_value=0.0)
        Carnivore().set_params({'beta': 1, 'F': 5})  # Carnivore gains all weight it eats,
        # and wants 5 food
        h_list = [Herbivore(weight=4)] * 2  # Creating list of 2 herbivores with weight 4
        c = Carnivore(weight=50)
        c.eat(h_list)
        assert not h_list
        assert c.w == 55
