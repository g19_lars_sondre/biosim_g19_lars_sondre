# -*- coding: utf-8 -*-

"""
Tests for the various landscape classes in the landscape.py module
"""

__author__ = 'John Sondre Sikkeland, Lars-Petter Andersen'
__email__ = 'johnsikk@nmbu.no, larsand@nmbu.no'

import pytest
from biosim.landscape import *
from biosim.animals import *


def test_landscape_has_subclasses():
    """ Testing that the  five landscapes types are subclasses of the landscape class"""
    assert issubclass(Jungle, Landscape)
    assert issubclass(Savannah, Landscape)
    assert issubclass(Desert, Landscape)
    assert issubclass(Ocean, Landscape)
    assert issubclass(Mountain, Landscape)


def test_all_landscape_types_has_habitable_flag():
    """ All landscape types should have class-attribute habitable set to right truth value"""
    assert Savannah().habitable
    assert Jungle().habitable
    assert Desert().habitable
    assert not Ocean().habitable
    assert not Mountain().habitable


class TestLandscape:
    """ Small basic unit tests for the landscape class """
    def test_landscape_attribute_habitable(self):
        """ Test that Landscape has class attribute habitable = False by default"""
        land = Landscape()
        assert not land.habitable

    def test_landscape_has_animal_list_and_no_fodder(self):
        """ The Landscape superclass does have a fodder parameter with 0 food, and lists
        for animals"""
        land = Landscape()
        assert land.fodder == 0
        assert not land.h_pop
        assert not land.c_pop
        assert not land.move_north
        assert not land.move_east
        assert not land.move_south
        assert not land.move_west

    def test_landscape_has_methods(self):
        land = Landscape()
        land.regrowth()
        land.animals_eat()
        land.animals_procreate()
        land.animals_migrate()
        land.animals_age_and_lose_weight()
        land.animals_die()


class TestJungleUnits:
    """ Unittests for the jungle landscape """
    def test_constructor(self):
        """ Checking that a instance can be created """
        j = Jungle()
        assert isinstance(j, Jungle)

    def test_constructor_default(self):
        """ Checking that the jungle class has a class attribute fodder (
        f_max) with the default value (800.0)"""
        j = Jungle()
        j.set_params(j.default_params)
        assert j.f_max == 800.0

    def test_jungle_has_fodder(self):
        """ Checking that a new jungle instance has fodder equal to the
        initial amount"""
        j = Jungle()
        assert j.fodder == j.f_max


class TestSavannahUnits:
    def test_savannah_constructor(self):
        """ Checking that a instance can be created """
        s = Savannah()
        assert isinstance(s, Savannah)

    def test_savannah_constructor_default_values(self):
        """ Checking that the savannah class has a class attribute fodder_max (f_max) with the
        default value of (300.0). Also checks that it contains alpha = 0.3"""
        s = Savannah()
        s.set_params(s.default_params)
        assert s.f_max == 300.0
        assert s.alpha == 0.3

    def test_savannah_has_fodder(self):
        """ Checking that a new savannah instance has fodder equal to the
        initial amount"""
        s = Savannah()
        assert s.fodder == s.f_max

    def test_savannah_regrowth(self):
        """ Place a herbivore on a savannah, make it eat and make sure savannah next year has
        less food"""
        s = Savannah()
        assert s.fodder == 300
        s.add_animal('Herbivore')
        s.animals_eat()
        assert s.fodder == 290
        s.regrowth()
        assert s.fodder == 293  # Calculated value by hand with standard parameters


class TestHabitableLandIntegration:
    """ Test to ensure Habitable land (Jungle, Savannah, Desert) properly handles animals"""
    def test_habitable_land_can_have_animals(self):
        """ Checking that animals can "live" on habitable land """
        for l in [Jungle(), Savannah(), Desert()]:
            l.add_animal('Herbivore')
            l.add_animal('Carnivore')
            assert isinstance(l.h_pop[0], Herbivore)
            assert isinstance(l.c_pop[0], Carnivore)

    def test_habitable_land_can_have_many_animals(self):
        """ Checking that more than one animal can live in a habitable tile, and that the
        animals are unique"""
        for j in [Jungle(), Savannah(), Desert()]:
            for _ in range(100):
                j.add_animal('Herbivore')
                j.add_animal('Carnivore')
            assert j.nh == 100
            assert j.nc == 100
            assert j.h_pop[0] is not j.h_pop[1]
            assert j.c_pop[0] is not j.h_pop[1]

    def test_habitable_land_sorts_animals(self):
        """ Checking that the habitable landscape types sorts animals properly"""
        for j in [Jungle(), Savannah(), Desert()]:
            for _ in range(50):
                j.add_animal('Herbivore')
                j.add_animal('Carnivore')
            j.sort_by_fitness(j.h_pop)
            j.sort_by_fitness(j.c_pop)
            for idx, animal in enumerate(j.h_pop[:-1]):  # Herbivores sorted?
                assert animal.phi > j.h_pop[idx+1].phi
            for idx, animal in enumerate(j.c_pop[:-1]):  # Carnivores sorted?
                assert animal.phi > j.c_pop[idx+1].phi

    def test_habitable_land_animals_can_eat(self):
        """ Checking that a herbivore can eat from landscapes with fodder (Jungle, Savannah)"""
        for j in [Jungle(), Savannah()]:
            ini_fodder = j.fodder
            j.add_animal('Herbivore')
            j.add_animal('Carnivore')
            j.animals_eat()
            assert j.fodder != ini_fodder

    def test_habitable_land_returns_animal_counts(self):
        """ Checking the methods to return animal counts"""
        num_herbs = 100
        num_carns = 101
        for l in [Jungle(), Savannah(), Desert()]:
            for _ in range(num_herbs):
                l.add_animal('Herbivore')
            for _ in range(num_carns):
                l.add_animal('Carnivore')
            assert l.nh == num_herbs
            assert l.nc == num_carns

    def test_habitable_land_animals_try_to_eat(self, mocker):
        """ Test that animals eat methods is called proper amount of times"""
        mocker.spy(Herbivore, 'eat')
        mocker.spy(Carnivore, 'eat')
        ini_herbivores = 50
        ini_carnivores = 30
        for j in [Jungle(), Savannah()]:
            for _ in range(ini_herbivores):
                j.add_animal('Herbivore')
            for _ in range(ini_carnivores):
                j.add_animal('Carnivore')
            j.animals_eat()
        assert Herbivore.eat.call_count == 2*ini_herbivores
        assert Carnivore.eat.call_count == 2*ini_carnivores

    def test_habitable_land_animals_must_be_sorted_after_eat(self):
        """ This is more a self check rather than a test. If this test ever fail then the
        herbivore lists must be sorted also after eating (should normally not change the order of
        fitness"""
        for j in [Jungle(), Savannah(), Desert()]:
            for _ in range(30):
                j.add_animal('Herbivore', age=100)
                j.add_animal('Carnivore', age=100)
            j.sort_by_fitness(j.h_pop)
            j.sort_by_fitness(j.c_pop)
            j.animals_eat()
            for idx, animal in enumerate(j.h_pop[:-1]):  # Herbivores sorted?
                assert animal.phi > j.h_pop[idx+1].phi
            for idx, animal in enumerate(j.c_pop[:-1]):  # Carnivore sorted?
                assert animal.phi > j.c_pop[idx+1].phi

    def test_habitable_land_animals_can_give_birth(self):
        """ Checking that animals can give birth to offsprings in habitable landscape """
        ini_animals = 100
        Herbivore.set_params(Herbivore.default_params)
        Carnivore.set_params(Carnivore.default_params)
        for j in [Jungle(), Savannah(), Desert()]:
            for _ in range(ini_animals):  # 100 animals should give a nice possibility of making a
                # baby-animal
                j.add_animal('Herbivore', weight=50)  # Only heavy animals can give birth
                j.add_animal('Carnivore', weight=50)
            assert j.nh == ini_animals
            assert j.nc == ini_animals
            j.animals_procreate()
            assert j.nh > ini_animals
            assert j.nc > ini_animals

    def test_habitable_landscape_all_animals_try_to_die(self, mocker):
        """ Check that animals try to die in the habitable landscape types"""
        mocker.spy(Animal, 'die')
        for j in [Savannah(), Desert(), Jungle()]:
            ini_animals = 100
            for _ in range(ini_animals):
                j.add_animal('Herbivore')
            j.animals_die()
        assert Animal.die.call_count == ini_animals * 3

    def test_habitable_land_animals_can_die(self):
        """ Checking that the amount of animals in the habitable landscapes will decrease after
        j.herbivore_dies() is called
        """
        for j in [Jungle(), Savannah(), Desert()]:
            for _ in range(100):  # out of 100 animals at least some should die
                j.add_animal('Herbivore')
                j.add_animal('Carnivore')
            assert j.nh == 100
            assert j.nc == 100
            j.animals_die()
            assert j.nh < 100
            assert j.nc < 100

    def test_animals_ages(self):
        """ Checking that animals age when j.herbivores_ages and j.carnivores_ages is called"""
        for j in [Jungle(), Savannah(), Desert()]:
            for _ in range(5):
                j.add_animal('Herbivore')
                j.add_animal('Carnivore')
            j.animals_age_and_lose_weight()
            for herbivore in j.h_pop:
                assert herbivore.a == 1
            for carnivore in j.c_pop:
                assert carnivore.a == 1
            j.animals_age_and_lose_weight()
            for herbivore in j.h_pop:
                assert herbivore.a == 2
            for carnivore in j.c_pop:
                assert carnivore.a == 2

    def test_animals_move_correctly(self):
        """Test to check that no animals get lost after calling .animals_migrate() method"""
        for j in [Jungle(), Savannah(), Desert()]:  # For all the habitable landscape types
            ini_animals = 100  # Adding 100 animals of each type
            for _ in range(ini_animals):
                j.add_animal(a_type='Herbivore',
                             weight=50)  # Heavy, so they have high fitness
                j.add_animal(a_type='Carnivore',
                             weight=50)
            tot_animals = len(j.h_pop +
                              j.c_pop +
                              j.move_north +
                              j.move_east +
                              j.move_south +
                              j.move_west)
            j.animals_migrate()
            animals_left = len(j.h_pop +
                               j.c_pop +
                               j.move_north +
                               j.move_east +
                               j.move_south +
                               j.move_west)
            assert tot_animals == animals_left

    def test_habitable_land_rejects_unknown_animal(self):
        """ Checking that calling method add_animal with an unknown animal raises ValueError"""
        s = Savannah()
        with pytest.raises(ValueError):
            s.add_animal('Fish')

    def test_habitable_land_cant_add_negative_age(self):
        """ Checking that calling method add_animal with age below 0 raises ValueError"""
        s = Savannah()
        with pytest.raises(ValueError):
            s.add_animal(a_type='Herbivore', age=-1)

    def test_habitable_land_cant_add_negative_weight(self):
        """ Checking that calling method add_animal with weight below 0 raises ValueError"""
        s = Savannah()
        with pytest.raises(ValueError):
            s.add_animal(a_type='Herbivore', weight=-1)

    @pytest.fixture
    def set_params_savannah(self, request):
        """
        Fixture setting class parameters on Savannah.

        The fixture sets Savannah parameters when they are called for setup,
        and resets them when called for teardown. This ensures that modified
        parameters are always reset before leaving a test.

        This fixture should be called via parametrize with indirect=True.

        Based on https://stackoverflow.com/a/33879151

        :param request: Request object automatically is provided by pytest.
        request.param is the parameter dictionary to be passed to
        Savannah.set_params()
        """
        Savannah.set_params(request.param)
        yield
        Savannah.set_params(Savannah.default_params)


class TestInhabitableLand:
    def test_inhabitable_land_no_fodder(self):
        """ Checking that Oceans and Mountains have no fodder """
        for l in [Ocean(), Mountain()]:
            assert not l.fodder

    def test_inhabitable_land_cant_have_animals(self):
        """ Checking that animals can not be added to habitable land """
        for l in [Ocean(), Mountain()]:
            with pytest.raises(ValueError):
                l.add_animal('Herbivore')
