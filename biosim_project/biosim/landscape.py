# -*- coding: utf-8 -*-

"""
This module provides classes implementing the various landscape types on
the island "Rossumøya"
"""

__author__ = 'John Sondre Sikkeland, Lars-Petter Andersen'
__email__ = 'johnsikk@nmbu.no, larsand@nmbu.no'

from .animals import Herbivore, Carnivore


class Landscape:
    """
    Rossumøya is divided into squares, here called tiles. Each square has a landscape type
    the class implements the idea of a landscape tile. The class has five subclasses, where each
    subclass represents one landscape type.
    The parameters in the subclasses has default values. The simulations can be carried out without
    the need to adjust the parameters. It is possible to change a subset of the parameters by
    providing a dictionary with only those
    parameters that are to be changed carrying out the parametrization. The parametrization methods
    reports an error if a dictionary contains unknown parameters, such as "n".
    The parametrization methods guards against illegal parameter values, such as f_max < 0.
    Any errors detected in the parameter dictionaries raises a ValueError.
    """

    habitable = False

    def __init__(self):
        """Create a landscape tile."""
        self.h_pop = []
        self.c_pop = []
        self.move_north = []
        self.move_east = []
        self.move_south = []
        self.move_west = []
        self.fodder = 0

    @property
    def nh(self):
        """Return number of herbivores in tile."""
        return len(self.h_pop)

    @property
    def nc(self):
        """Return number of carnivores in tile"""
        return len(self.c_pop)

    def regrowth(self):
        pass

    def animals_eat(self):
        """ All herbivores eat, the fittest first. Then the carnivores eats, the fittest first."""
        self.sort_by_fitness(self.h_pop)
        for h in self.h_pop:
            self.fodder = h.eat(f_available=self.fodder)
        self.sort_by_fitness(self.c_pop)
        for c in self.c_pop:
            c.eat(self.h_pop)

    @staticmethod
    def sort_by_fitness(population_list):
        """ Method to sort an animal list by fitness"""
        population_list.sort(key=lambda x: x.phi, reverse=True)

    def animals_procreate(self):
        """
        The animals procreate, and the babies are added to the population.
        """
        self.add_babies(self.h_pop)  # Herbivores procreate
        self.add_babies(self.c_pop)  # Carnivores procreate

    @staticmethod
    def add_babies(animal_list):
        """
        Method that adds the newborn babies to the population
        :param animal_list: List with animals
        """
        n_animals = len(animal_list)
        babies = []
        for animal in animal_list:
            baby = animal.give_birth(number_of_similar_animals=n_animals)
            if baby is not None:
                babies.append(baby)
        animal_list.extend(babies)

    def animals_migrate(self):
        """
        Each landscape will call the animals migrate() method. If the animal dont want to
        move, nothing happens, but if the animal wants to move, it will "emigrate" in a
        direction, either
        1: Move up
        2: Move right
        3: Move down
        4: Move left
        """
        for herbivore in reversed(self.h_pop):
            direction = herbivore.migrate()
            if direction:
                self.emigrate(herbivore, direction)
                self.h_pop.remove(herbivore)
        for carnivore in reversed(self.c_pop):
            direction = carnivore.migrate()
            if direction:
                self.emigrate(carnivore, direction)
                self.c_pop.remove(carnivore)

    def emigrate(self, animal, direction):
        """
        This function collects animals that wants to move
        :param animal: The animal that is moving
        :param direction: The direction the animal wants to move, where:
        1 - North
        2 - East
        3 - South
        4 - West
        """
        if direction == 1:
            self.move_north.append(animal)
        elif direction == 2:
            self.move_east.append(animal)
        elif direction == 3:
            self.move_south.append(animal)
        else:
            self.move_west.append(animal)

    def animals_age_and_lose_weight(self):
        """
        Animals gets older, and lose weight
        """
        for animal in self.h_pop + self.c_pop:  # For all animals
            animal.age_and_lose_weight()

    def animals_die(self):
        """
        Animals die. If animal do not die, they are added to a list of living animals.
        """
        self.h_pop = [herbivore for herbivore in self.h_pop if not herbivore.die()]
        self.c_pop = [carnivore for carnivore in self.c_pop if not carnivore.die()]

    def add_animal(self, a_type, age=0, weight=None):
        """
        Adds animals to the landscape types they can reside
        :param a_type: The animals types on the island, Herbivore and Carnivore. If there is found
        an animal which is not a Herbivore or carnivore, it raises a ValueError.
        :param age: Animal has an age. If age is a negative integer, it raises a valueError.
        :param weight: Animal has a weight. If weight is negative, it raises a ValueError
        """
        if not self.habitable:
            raise ValueError('Landscape type "{}" is not habitable!'
                             .format(self.__class__.__name__))
        if a_type not in ('Herbivore', 'Carnivore'):
            raise ValueError('Unknown animal "{}". Supported animal types are "Herbivore" or'
                             ' "Carnivore".'.format(a_type))
        if type(age) is not int or age < 0:
            raise ValueError('Age has to be a non-negative integer')
        if weight is not None:
            if weight < 0:
                raise ValueError('Weight must be positive!')
        if a_type == 'Herbivore':
            self.h_pop.append(Herbivore(age=age, weight=weight))
        if a_type == 'Carnivore':
            self.c_pop.append(Carnivore(age=age, weight=weight))


class Ocean(Landscape):
    """
    A passive tile surrounding the island. Oceans can not contain animals. This prohibits
    animal migration
    """
    def __init__(self):
        super().__init__()


class Mountain(Landscape):
    """Area that has steep terrain and frightens animals. Animals can not reside here."""
    def __init__(self):
        super().__init__()


class Jungle(Landscape):
    """
    Landscape type where the growth of vegetation is so quick that it is
    not susceptible to long term damage due to overgrazing. Animals can reside here.
    """
    habitable = True
    # This parameter is defined at class level
    f_max = 800.0
    default_params = {'f_max': f_max}  # Default amount of food in the jungle

    @classmethod
    def get_params(cls):
        """
        Gets jungle parameter
        :return: Dict, class parameter
        """
        return {'f_max': cls.f_max}

    @classmethod
    def set_params(cls, new_params):
        """
        Class method to set the new parameters to the jungle class
        :param new_params: Dictionary with legal keys 'f_max'
        :Raises: ValueError, KeyError
        """
        for key in new_params:
            if key not in cls.default_params:
                raise KeyError('Invalid parameter name ' + key)

        if 'f_max' in new_params:
            if new_params['f_max'] <= 0:
                raise ValueError('Jungle can not have negative amount of fodder')
            cls.f_max = new_params['f_max']

    def __init__(self):
        super().__init__()
        self.fodder = self.f_max

    def regrowth(self):
        """New fodder regrows at the start of each year."""
        self.fodder = self.f_max


class Savannah(Landscape):
    """
    Landscape type offering limited amount of fodder for the Herbivores and is sensitive to
    overgrazing. The carnivores can hunt herbivores in this landscape type.
    """
    habitable = True

    # These parameters are defined at class level
    f_max = 300.0   # Standard amount of fodder in savannah
    alpha = 0.3     # Default growth rate of food in the savannah
    default_params = {'f_max': 300.0,
                      'alpha': 0.3}     # Storing the default parameters

    @classmethod
    def get_params(cls):
        """
        Gets savannah parameters
        :return: Dict, class parameter
        """
        return {'f_max': cls.f_max, 'alpha': cls.alpha}

    @classmethod
    def set_params(cls, new_params):
        """
        Class method to set new parameters to Savannah class
        :param new_params: Dictionary with legal keys 'f_max', 'alpha'
        :raises: KeyError, ValueError
        """
        for key in new_params:
            if key not in cls.default_params:
                raise KeyError('Invalid parameter name ' + key)

        if 'f_max' in new_params:
            if new_params['f_max'] <= 0:
                raise ValueError('Savannah can not have negative amount of fodder')
            cls.f_max = new_params['f_max']

        if 'alpha' in new_params:
            if not 0 <= new_params['alpha'] <= 1:
                raise ValueError('Savannah can not regrow')
            cls.alpha = new_params['alpha']

    def __init__(self):
        super().__init__()
        self.fodder = self.f_max

    def regrowth(self):
        """
        Regrowth in the savannah:
        fodder = fodder + alpha * (max_fodder - fodder)
        """
        self.fodder = self.fodder + self.alpha * (self.f_max - self.fodder)


class Desert(Landscape):
    """
    Landscape type where animals might reside, but contains no fodder for the herbivores.
    The carnivores can hunt and eat herbivores.
    """
    habitable = True  # Animals can stay inside the desert

    def __init__(self):
        super().__init__()
