# -*- coding: utf-8 -*-

"""
:mod:'biosim.simulation' provides the user interface to this package.

Each simulation is represented by a :class:'Island' instance. On each
instance, the :meth:'Biosim.simulate' method can be called as often as
you like to simulate a given number of years at the island.

The simulation comes with a simple visualization:
* A map of the island;
* Population maps showing the distribution of Herbivores and Carnivores on the island;
* Line graph of the total number of animals on the island, represented with one line for each
animal type.;
* Information on years passed, current animals on the island, and animals per type is displayed.;

The simulation also provides a simple graphical user interface (GUI) that enables users to:
* Modify omega and gamma for Herbivores and Carnivores;
* Modify f_max in jungle/savannah and alpha in savannah;
* Reset parameters to initial value (Values set before first BioSim.simulation() call);
* Pause/Run simulation;
* Interrupt running simulation.;

When running :meth:'Biosim.simulate' the state of the system is visualized as the simulation runs,
at intervals that can be chosen when calling the simulation. It is also possible to save the
graphics to file at regular intervals. By calling :meth:'BioSim.make_movie' after a simulation is
complete, individual graphics files can be combined into an animation.
.. note::
   * In order to make movies, this module requires the program "ffmpeg", which is available from
   '<http://ffmpeg.org>'.
   * The :const:'_FFMPEG_BINARY' constant below has to be set to the command required to invoke
   the program.

Example
--------
::
    ini_herbs = [{'loc': (2, 2),
                  'pop': [{'species': 'Herbivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(10)]}]
    ini_carns = [{'loc': (2, 3),
                  'pop': [{'species': 'Carnivore',
                           'age': 5,
                           'weight': 20}
                          for _ in range(10)]}]
    sim = BioSim(island_map="OOOO\nOJJO\nOOOO",
                 ini_pop=ini_herbs,
                 seed=12345,
                 img_base=".\simulation")
    sim.simulate(num_years=10, vis_years=1)
    sim.add_population(population=ini_carns)
    sim.simulate(num_years=10, vis_years=1)
    sim.make_movie()

This code
#. Will create a 4x3 island with two jungle tiles surrounded by ocean, place 10 herbivores on the
   island and simulate for 10 years.
#. Then 10 carnivores are placed on location (2, 3) and then
   again 10 years is simulated.
#. performs a simulation of 50 steps, updating the graphics after each
   step and saving a figure after each 5th step.
#. creates a movie from the individual figures saved.
#. The graphics and movies will be saved in the folder you run this script from with file names
simulation00000.png, simulation00001.png and so on.
#. The movie is stored as img_base + movie_fmt.
#. The only movie format supported is 'mp4'.
"""

__author__ = 'John Sondre Sikkeland, Lars-Petter Andersen'
__email__ = 'johnsikk@nmbu.no, larsand@nmbu.no'


from .island import Island
from .animals import *
from .landscape import Jungle, Savannah
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Button, Slider
import subprocess

# update this variable to point to your ffmpeg binary
_FFMPEG_BINARY = r'C:\Users\larsp\Downloads\ffmpeg-20190120-62f8d27-win64-static\ffmpeg-20190120' \
                 r'-62f8d27-win64-static\bin\ffmpeg.exe'


class BioSim:
    """Provides user interface for the population dynamics simulation, including visualization."""

    def __init__(self, island_map, ini_pop, seed, ymax_animals=None, cmax_animals=None,
                 img_base=None, img_fmt='png'):
        """
        :param island_map: Multi-line string specifying island geography
        :param ini_pop: List of dictionaries specifying initial population
        :param seed: Integer used as random number seed
        :param ymax_animals: Number specifying y-axis limit for graph showing animal numbers
        :param cmax_animals: Dict specifying color-code limits for animal densities
        :param img_base: String with beginning of file name for figures, including path
        :param img_fmt: String with file type for figures, e.g. ’png’
        If ymax_animals is None, the y-axis limit should be adjusted automatically.
        If cmax_animals is None, sensible, fixed default values should be used.
        cmax_animals is a dict mapping species names to numbers, e.g.,
        {’Herbivore’: 50, ’Carnivore’: 20}
        If img_base is None, no figures are written to file.
        File names are formed as
        ’{}_{:05d}.{}’.format(img_base, img_no, img_fmt)
        where img_no are consecutive image numbers starting from 0.
        img_base should contain a path and beginning of a file name.
        """
        random.seed(seed)  # Setting the seed for the simulation
        self._island = Island(island_map=island_map, ini_pop=ini_pop)
        self._island_map = island_map
        self._n_rows = len(island_map.splitlines())
        self._n_columns = len(island_map.splitlines()[0])

        self._img_base = img_base  # This will be set to none if not provided
        self._img_fmt = img_fmt  # 'png' by default

        self._year = 0  # A simulation always starts at year 0
        self._final_year = None
        self._img_ctr = 0  # Images will be numerated starting from 00000

        # Following attributes will be initialized by _setup_graphics
        self._fig = None
        self._map_ax = None
        self._img_axis = None
        self._animal_ax = None
        self._herbivore_line = None
        self._carnivore_line = None
        self._herbivore_ax = None
        self._herbivore_axis = None
        self._carnivore_ax = None
        self._carnivore_axis = None
        self._max_animals = None
        self._island_info_ax = None
        self._island_info_txt = None

        self._ax_slider_1 = None  # Herbivore omega
        self._slwidth = 0.08  # Width of sliders and buttons
        self._spos1 = 0.6  # x-placement of sliders col 1
        self._spos2 = 0.8  # x-placement of sliders col 2
        self._slider_1 = None
        self._ax_slider_2 = None  # Herbivore gamma
        self._slider_2 = None
        self._ax_slider_3 = None  # Carnivore omega
        self._slider_3 = None
        self._ax_slider_4 = None  # Carnivore gamma
        self._slider_4 = None
        self._ax_slider_5 = None  # Jungle f_max
        self._slider_5 = None
        self._ax_slider_6 = None  # Savannah f_max
        self._slider_6 = None
        self._ax_slider_7 = None  # Savannah alpha
        self._slider_7 = None
        self._ax_pause = None
        self._w_pause = None
        self._ax_interrupt = None
        self._w_interrupt = None
        self._ax_reset = None
        self._w_reset = None

        self._cmap = plt.cm.get_cmap('viridis')
        self._cmap.set_under(color='grey')

        self._paused = False  # Flag to handle pause/run button
        self._interrupt = False  # Flag to interrupt loop

        if ymax_animals is not None:
            self._ymax_animals = ymax_animals
        else:
            self._ymax_animals = None

        if cmax_animals is not None:
            self._cmax_herbivore = cmax_animals['Herbivore']
            self._cmax_carnivore = cmax_animals['Carnivore']
        else:
            self._cmax_herbivore = 200
            self._cmax_carnivore = 50

    @staticmethod
    def set_animal_parameters(species, params):
        """
        Set parameters for animal species.
        :param species: String, name of animal species
        :param params: Dict with valid parameter specification for species
        """
        if species == 'Herbivore':
            Herbivore.set_params(params)
        elif species == 'Carnivore':
            Carnivore.set_params(params)
        else:
            raise ValueError('Unknown species type: ', species)

    @staticmethod
    def set_landscape_parameters(landscape, params):
        """
        Set parameters for landscape type.
        :param landscape: String, code letter for landscape
        :param params: Dict with valid parameter specification for landscape
        """
        if landscape == 'J':
            Jungle.set_params(params)
        elif landscape == 'S':
            Savannah.set_params(params)
        else:
            raise ValueError('Unknown landscape type: ', landscape)

    def simulate(self, num_years, vis_years=1, img_years=None):
        """
        Run simulation while visualizing the result.
        :param num_years: number of years to simulate
        :param vis_years: years between visualization updates
        :param img_years: years between visualizations saved to files (default: vis_years)
        Image files will be numbered consecutively.
        """
        if img_years is None:
            img_years = vis_years

        self._final_year = self._year + num_years
        self._setup_graphics()

        while self.year < self._final_year:
            if self.year % vis_years == 0:
                self._update_graphics()

            if self.year % img_years == 0:
                self._save_graphics()

            self._island.one_year()
            self._year += 1

            if self._interrupt:  # Stops simulation if Interrupt button clicked
                break
            while self._paused:
                plt.pause(0.05)

        self._interrupt = False  # Enabling further simulation

    def add_population(self, population):
        """
        Add a population to the island
        :param population: List of dictionaries specifying population
        Example
        --------
        ::
            ini_herbs = [{'loc': (2, 2),
                        'pop': [{'species': 'Herbivore',
                                 'age': 5,
                                 'weight': 20}
                                    for _ in range(10)]}]
            sim = BioSim(island_map="OOOO\nOJJO\nOOOO",
                         ini_pop=[],
                         seed=12345)
            sim.simulate(num_years=1)
            sim.add_population(population=ini_herbs)
            sim.simulate(num_years=10)

        This code
        #. Will create a 4x3 island with two jungle tiles surrounded by ocean.
        #. Will run a simulation of a single year without animals on the island.
        #. It will then place 10 herbivores on the island with age 5 and weight 20.
        #. Will again simulate for another 10 years.
        """
        self._island.add_population(population=population)

    @property
    def year(self):
        """Last year simulated."""
        return self._year

    @property
    def num_animals(self):
        """Total number of animals on island."""
        return sum(self._island.animal_counts())

    @property
    def num_animals_per_species(self):
        """Number of animals per species in island, as dictionary."""
        num_herbivores, num_carnivores = self._island.animal_counts()
        return {'Herbivore': num_herbivores,
                'Carnivore': num_carnivores}

    @property
    def animal_distribution(self):
        """Pandas DataFrame with animal count per species for each tile on island."""
        data = self._island.animals_per_tile()
        return pd.DataFrame(data=data, columns=['Row', 'Col', 'Herbivore', 'Carnivore'])

    def make_movie(self, movie_fmt='mp4'):
        """
        Creates MPEG4 movie from visualization images saved.

        .. :note:
            Requires ffmpeg

        The movie is stored as img_base + movie_fmt (Only mp4 is supported in the current version)
        """

        if self._img_base is None:
            raise RuntimeError("No filename defined.")

        if movie_fmt == 'mp4':
            try:
                # Parameters chosen according to http://trac.ffmpeg.org/wiki/Encode/H.264,
                # section "Compatibility"
                subprocess.check_call([_FFMPEG_BINARY,
                                       '-i', '{}_%05d.png'.format(self._img_base),
                                       '-y',
                                       '-profile:v', 'baseline',
                                       '-level', '3.0',
                                       '-pix_fmt', 'yuv420p',
                                       '{}.{}'.format(self._img_base,
                                                      movie_fmt)])
            except subprocess.CalledProcessError as err:
                raise RuntimeError('ERROR: ffmpeg failed with: {}'.format(err))
        else:
            raise ValueError('Unknown movie format: ' + movie_fmt)

    def _save_graphics(self):
        """Saves graphics to file if file name is given."""

        if self._img_base is None:
            return

        plt.savefig('{base}_{num:05d}.{type}'.format(base=self._img_base,
                                                     num=self._img_ctr,
                                                     type=self._img_fmt))
        self._img_ctr += 1  # Image counter += 1

    def _setup_graphics(self):
        """ Creates subplots and makes them ready to be used"""

        # Step 1: Create a figure window:
        if self._fig is None:
            self._fig = plt.figure(figsize=(18, 12))

        # Add top left subplot for the image of the island.
        if self._map_ax is None:
            #                                  llx    lly   w    h
            self._map_ax = self._fig.add_axes([0.05, 0.7, 0.25, 0.25])  # Upper left plot:
            rgb_values = {'O': (0.0, 0.0, 1.0),  # blue
                          'M': (0.4, 0.4, 0.4),  # grey
                          'J': (0.0, 0.6, 0.0),  # dark green
                          'S': (0.5, 1.0, 0.5),  # light green
                          'D': (0.8, 0.8, 0.3)}  # light yellow
            self._map_axis = [[rgb_values[column] for column in row]
                              for row in self._island_map.splitlines()]
            self._map_ax.imshow(self._map_axis)
            self._map_ax.set_xticks(range(0, 1 + self._n_columns, 5))
            self._map_ax.set_xticklabels(range(1, 1 + self._n_columns, 5))
            self._map_ax.set_yticks(range(0, 1 + self._n_rows, 5))
            self._map_ax.set_yticklabels(range(1, 1 + self._n_rows, 5))
            self._map_axlg = self._fig.add_axes([0.280, 0.7, 0.08, 0.25])
            self._map_axlg.axis('off')
            for ix, name in enumerate(('Ocean', 'Mountain', 'Jungle',
                                       'Savannah', 'Desert')):
                self._map_axlg.add_patch(plt.Rectangle((0.15, ix * 0.2), 0.1, 0.1,
                                                       edgecolor='black',
                                                       facecolor=rgb_values[name[0]]))
                self._map_axlg.text(0.35, ix * 0.2, name, transform=self._map_axlg.transAxes)
            self._map_ax.set_title("Island map")

        # Add top right subplot for line graph of herbivore and carnivore population
        if self._animal_ax is None:
            #                                     llx    lly   w    h
            self._animal_ax = self._fig.add_axes([0.4, 0.4, 0.5, 0.5])  # Upper right plot:

            if self._ymax_animals is not None:
                self._animal_ax.set_ylim(0, self._ymax_animals)
            else:
                self._max_animals = self.num_animals
                self._animal_ax.set_ylim(0, self._max_animals * 1.1)
            self._animal_ax.set_title("Population\n\n ")  # Is there a better way?
            self._animal_ax.set_xlabel("# Years")
            self._animal_ax.set_ylabel("# animals")

        self._animal_ax.set_xlim(0, self._final_year + 1)

        #  HERBIVORES LINE
        if self._herbivore_line is None:
            herbivore_plot = self._animal_ax.plot(np.arange(0, self._final_year),
                                                  np.full(self._final_year, np.nan),
                                                  label='Herbivores')
            self._herbivore_line = herbivore_plot[0]
        else:
            xdata, ydata = self._herbivore_line.get_data()
            xnew = np.arange(xdata[-1] + 1, self._final_year)
            if len(xnew) > 0:
                ynew = np.full(xnew.shape, np.nan)
                self._herbivore_line.set_data(np.hstack((xdata, xnew)),
                                              np.hstack((ydata, ynew)))

        #  CARNIVORES LINE
        if self._carnivore_line is None:
            carnivore_plot = self._animal_ax.plot(np.arange(0, self._final_year),
                                                  np.full(self._final_year, np.nan),
                                                  label='Carnivores')
            self._carnivore_line = carnivore_plot[0]
        else:
            xdata, ydata = self._carnivore_line.get_data()
            xnew = np.arange(xdata[-1] + 1, self._final_year)
            if len(xnew) > 0:
                ynew = np.full(xnew.shape, np.nan)
                self._carnivore_line.set_data(np.hstack((xdata, xnew)),
                                              np.hstack((ydata, ynew)))

        # Add left middle subplot for heat map of herbivore distribution
        if self._herbivore_ax is None:
            #                                        llx    lly   w    h
            self._herbivore_ax = self._fig.add_axes([0.05, 0.4, 0.25, 0.25])  # Middle left plot
            self._herbivore_ax.set_xticks(range(0, 1 + self._n_columns, 5))
            self._herbivore_ax.set_xticklabels(range(1, 1 + self._n_columns, 5))
            self._herbivore_ax.set_yticks(range(0, 1 + self._n_rows, 5))
            self._herbivore_ax.set_yticklabels(range(1, 1 + self._n_rows, 5))
            self._herbivore_ax.set_title("Herbivore distribution")

        if self._herbivore_axis is None:
            self._herbivore_axis = self._herbivore_ax.imshow(np.reshape(self.animal_distribution[
                                                                            'Herbivore'].values,
                                                                        newshape=(self._n_rows,
                                                                                  self._n_columns)),
                                                             interpolation='none',
                                                             cmap=self._cmap,
                                                             vmin=0.9, vmax=self._cmax_herbivore)
            plt.colorbar(mappable=self._herbivore_axis)
        else:
            self._herbivore_axis.set_data(np.reshape(self.animal_distribution['Herbivore'].values,
                                                     newshape=(self._n_rows, self._n_columns)))

        # Add lower left subplot for heat map of carnivore distribution
        if self._carnivore_ax is None:
            #                                        llx    lly   w    h
            self._carnivore_ax = self._fig.add_axes([0.05, 0.1, 0.25, 0.25])  # Lower right plot:

            self._carnivore_ax.set_xticks(range(0, 1 + self._n_columns, 5))
            self._carnivore_ax.set_xticklabels(range(1, 1 + self._n_columns, 5))
            self._carnivore_ax.set_yticks(range(0, 1 + self._n_rows, 5))
            self._carnivore_ax.set_yticklabels(range(1, 1 + self._n_rows, 5))
            self._carnivore_ax.set_title("Carnivore distribution")

        if self._carnivore_axis is None:
            self._carnivore_axis = self._carnivore_ax.imshow(np.reshape(self.animal_distribution[
                                                                            'Carnivore'].values,
                                                                        newshape=(self._n_rows,
                                                                                  self._n_columns)),
                                                             interpolation='none',
                                                             cmap=self._cmap,
                                                             vmin=0.9, vmax=self._cmax_carnivore)
            plt.colorbar(mappable=self._carnivore_axis)
        else:
            self._carnivore_axis.set_data(np.reshape(self.animal_distribution[
                                                         'Carnivore'].values,
                                                     newshape=(self._n_rows, self._n_columns)))

        # Add text displaying information of the population on the island
        if self._island_info_ax is None:
            self._island_info_ax = self._fig.add_axes([0.35, 0.3, 0.25, 0.25])
            self._island_info_ax.axis('off')
            self._island_info_txt = 'Year: {:5}\n\n' \
                                    'Total Animals: {:6}\n\n' \
                                    'Herbivores: {:6}\n\n' \
                                    'Carnivores: {:6}'
            self._island_info_axis = \
                self._island_info_ax.text(0., 0.,
                                          self._island_info_txt.format(
                                              self.year,
                                              self.num_animals,
                                              self.num_animals_per_species['Herbivore'],
                                              self.num_animals_per_species['Carnivore']),
                                          horizontalalignment='left',
                                          verticalalignment='top',
                                          transform=self._island_info_ax.transAxes,
                                          fontsize=18)

        # Widget to change herbivore omega
        if self._ax_slider_1 is None:
            self._ax_slider_1 = self._fig.add_axes([self._spos1, 0.3, self._slwidth, 0.03])
            self._slider_1 = Slider(self._ax_slider_1,
                                    label='Herbivore omega',  # label for slider
                                    valmin=0, valmax=1,  # minimal, maximal value
                                    valinit=Herbivore.omega,  # initial value
                                    valfmt='%.2f')
            self._slider_1.on_changed(self._set_herbivore_omega)

        # Widget to change herbivore gamma
        if self._ax_slider_2 is None:
            self._ax_slider_2 = self._fig.add_axes([self._spos1, 0.25, self._slwidth, 0.03])
            self._slider_2 = Slider(self._ax_slider_2,
                                    label='Herbivore gamma',  # label for slider
                                    valmin=0, valmax=1,  # minimal, maximal value
                                    valinit=Herbivore.gamma,  # initial value
                                    valfmt='%.2f')
            self._slider_2.on_changed(self._set_herbivore_gamma)

        # Widget to change carnivore omega
        if self._ax_slider_3 is None:
            self._ax_slider_3 = self._fig.add_axes([self._spos1, 0.20, self._slwidth, 0.03])
            self._slider_3 = Slider(self._ax_slider_3,
                                    label='Carnivore omega',  # label for slider
                                    valmin=0, valmax=1,  # minimal, maximal value
                                    valinit=Carnivore.omega,  # initial value
                                    valfmt='%.2f')
            self._slider_3.on_changed(self._set_carnivore_omega)

        # Widget to change carnivore gamma
        if self._ax_slider_4 is None:
            self._ax_slider_4 = self._fig.add_axes([self._spos1, 0.15, self._slwidth, 0.03])
            self._slider_4 = Slider(self._ax_slider_4,
                                    label='Carnivore gamma',  # label for slider
                                    valmin=0, valmax=1,  # minimal, maximal value
                                    valinit=Carnivore.gamma,  # initial value
                                    valfmt='%.2f')
            self._slider_4.on_changed(self._set_carnivore_gamma)

        # Widget to change Jungle f_max
        if self._ax_slider_5 is None:
            self._ax_slider_5 = self._fig.add_axes([self._spos2, 0.15, self._slwidth, 0.03])
            self._slider_5 = Slider(self._ax_slider_5,
                                    label='Jungle f_max',  # label for slider
                                    valmin=0, valmax=1000,  # minimal, maximal value
                                    valinit=Jungle.f_max,  # initial value
                                    valfmt='%.1f')
            self._slider_5.on_changed(self._set_jungle_fmax)

        # Widget to change Savannah f_max
        if self._ax_slider_6 is None:
            self._ax_slider_6 = self._fig.add_axes([self._spos2, 0.25, self._slwidth, 0.03])
            self._slider_6 = Slider(self._ax_slider_6,
                                    label='Savannah f_max',  # label for slider
                                    valmin=0, valmax=500,  # minimal, maximal value
                                    valinit=Savannah.f_max,  # initial value
                                    valfmt='%.1f')
            self._slider_6.on_changed(self._set_savannah_fmax)

        # Widget to change Savannah alpha
        if self._ax_slider_7 is None:
            self._ax_slider_7 = self._fig.add_axes([self._spos2, 0.2, self._slwidth, 0.03])
            self._slider_7 = Slider(self._ax_slider_7,
                                    label='Savannah alpha',  # label for slider
                                    valmin=0, valmax=1,  # minimal, maximal value
                                    valinit=Savannah.alpha,  # initial value
                                    valfmt='%.1f')
            self._slider_7.on_changed(self._set_savannah_alpha)

        # Button to pause/run
        if self._ax_pause is None:
            self._ax_pause = self._fig.add_axes([self._spos1, 0.10, self._slwidth, 0.03])
            self._w_pause = Button(self._ax_pause, 'Pause/Run', hovercolor='0.975')
            self._w_pause.on_clicked(self._change_pause_status)

        # Button to interrupt
        if self._ax_interrupt is None:
            self._ax_interrupt = self._fig.add_axes([self._spos1, 0.05, self._slwidth, 0.03])
            self._w_interrupt = Button(self._ax_interrupt,
                                       'Interrupt',
                                       hovercolor='0.975')
            self._w_interrupt.on_clicked(self._stop_sim)

        # Button to reset sliders
        if self._ax_reset is None:
            self._ax_reset = self._fig.add_axes([self._spos2, 0.05, self._slwidth, 0.08])
            self._w_reset = Button(self._ax_reset,
                                   'Reset\nparameters',
                                   hovercolor='0.975')
            self._w_reset.on_clicked(self._reset)

    @staticmethod
    def _set_herbivore_omega(value):
        """
        Set parameter to new value
        :param value: New value for n from slider_1 widget.
        """
        Herbivore.set_params(new_params={'omega': value})

    @staticmethod
    def _set_herbivore_gamma(value):
        """
        Set parameter to new value
        :param value: New value for n from slider_2 widget.
        """
        Herbivore.set_params(new_params={'gamma': value})

    @staticmethod
    def _set_carnivore_omega(value):
        """
        Set parameter to new value
        :param value: New value for n from slider_3 widget.
        """
        Carnivore.set_params(new_params={'omega': value})

    @staticmethod
    def _set_carnivore_gamma(value):
        """
        Set parameter to new value
        :param value: New value for n from slider_4 widget.
        """
        Carnivore.set_params(new_params={'gamma': value})

    @staticmethod
    def _set_jungle_fmax(value):
        """
        Set parameter to new value
        :param value: New value for n from slider_5 widget.
        """
        Jungle.set_params(new_params={'f_max': value})

    @staticmethod
    def _set_savannah_fmax(value):
        """
        Set parameter to new value
        :param value: New value for n from slider_6 widget.
        """
        Savannah.set_params(new_params={'f_max': value})

    @staticmethod
    def _set_savannah_alpha(value):
        """
        Set parameter to new value
        :param value: New value for n from slider_7 widget.
        """
        Savannah.set_params(new_params={'alpha': value})

    def _change_pause_status(self, event):
        """
        Change self._paused flag when pause button is clicked
        """
        print('\nPause button clicked')
        print('    {}'.format(event))
        if self._paused:
            self._paused = False
        else:
            self._paused = True

    def _reset(self, event):
        """
        Reset slider values when clicked
        """
        print('\nReset button clicked')
        print('    {}'.format(event))
        self._slider_1.reset()
        self._slider_2.reset()
        self._slider_3.reset()
        self._slider_4.reset()
        self._slider_5.reset()
        self._slider_6.reset()
        self._slider_7.reset()

    def _stop_sim(self, event):
        """
        Change self._paused flag when pause button is clicked
        """
        print('\nInterrupt button clicked')
        print('    {}'.format(event))
        self._interrupt = True

    def _update_animal_ax(self):
        ydata = self._herbivore_line.get_ydata()
        ydata[self.year] = self.num_animals_per_species['Herbivore']
        self._herbivore_line.set_ydata(ydata)

        ydata = self._carnivore_line.get_ydata()
        ydata[self.year] = self.num_animals_per_species['Carnivore']
        self._carnivore_line.set_ydata(ydata)
        self._animal_ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
                               ncol=2, mode="expand", borderaxespad=0.)

    def _update_heatmap_axes(self):
        self._herbivore_axis.set_data(np.reshape(a=self.animal_distribution['Herbivore'].values,
                                                 newshape=(self._n_rows, self._n_columns)))
        self._carnivore_axis.set_data(np.reshape(a=self.animal_distribution['Carnivore'].values,
                                                 newshape=(self._n_rows, self._n_columns)))

    def _update_text(self):
        self._island_info_axis.set_text(self._island_info_txt.format(
            self.year,
            self.num_animals,
            self.num_animals_per_species['Herbivore'],
            self.num_animals_per_species['Carnivore']))

    def _update_graphics(self):
        """Updates graphics with current data."""
        self._update_animal_ax()
        self._update_heatmap_axes()
        self._update_text()
        # ylimit for the animal ax:
        if self._ymax_animals is None:
            if self.num_animals > self._max_animals:
                self._max_animals = self.num_animals
                self._animal_ax.set_ylim(0, self._max_animals + 100)
        plt.pause(1e-2)
