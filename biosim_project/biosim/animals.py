# -*- coding: utf-8 -*-

"""
:mod:'biosim.animal' provides the classes implementing the animal types on the island
"Rossumøya". There are two types of animals; Herbivores and Carnivores.
"""

__author__ = 'John Sondre Sikkeland, Lars-Petter Andersen'
__email__ = 'johnsikk@nmbu.no, larsand@nmbu.no'

from math import exp
import random


class Animal:
    """
    Parent class with common methods for herbivores and carnivores. This
    includes birth, age, breeding, migration, fitness, aging, gaining and losing
    weight. The only method that differs is the eat method, which is defined under the herbivore
    and carnivore subclasses.
    """

    def __init__(self, age=0, weight=None):
        """
        Creates an animal with age a (default 0) and weight w (based on standard parameters)
        :param age: The age of the animal.
        :param weight: The weight of the animal.
        note:: Trying to set animals weight to a negative number makes the animals weight become 0.
        """

        self.a = age
        if weight is None:
            bw = -1
            while bw < 0:
                bw = random.gauss(self.w_birth, self.sigma_birth)
                self.w = bw
        elif weight <= 0:
            self.w = 0
        else:
            self.w = weight
        self.phi = 0
        self.calculate_fitness()

    def calculate_fitness(self):
        """
        Calculates the fitness of the animal, based on the parameters phi_age, a_half,
        phi_weight, w_half and the animals weight and age. The animals fitness attribute .phi is
        updated.
        """
        if self.w <= 0:
            self.phi = 0
        else:
            q_plus = 1/(1 + exp(self.phi_age*(self.a - self.a_half)))
            q_minus = 1/(1 + exp(-self.phi_weight*(self.w - self.w_half)))
            self.phi = q_plus * q_minus

    def age_and_lose_weight(self):
        """
        Age increases by one year for each passing year, and the weight
        decreases by a factor eta.
        """
        self.a += 1
        self.w -= (self.eta * self.w)
        self.calculate_fitness()

    def give_birth(self, number_of_similar_animals):
        """
        Animals give birth
        :param number_of_similar_animals: Number of similar animals on the tile
        :return: None or a newborn animal
        """

        if self.w < self.zeta * (self.w_birth + self.sigma_birth):
            birth_probability = 0
        else:
            birth_probability = min(1, (self.gamma * self.phi * (number_of_similar_animals - 1)))
        if random.random() < birth_probability:  # If an animal is to be created:
            baby = self.__class__()
            if not (self.xi * baby.w) > self.w:  # If the mother will lose more than her own weight
                self.w -= (self.xi * baby.w)
                self.calculate_fitness()
                return baby

    def migrate(self):
        """
        When called, this method will first decide if the animal will try to move or not. An animal
        moves with probability mu * phi (fitness). If the animal does not move (random.random()
        return a high value), then nothing is returned. (None)
        If the animal wants to move, the direction will be determined and the method will return
        either:
        1 - Move north
        2 - Move east
        3 - Move south
        4 - Move west
        :return: None or int: 1, 2, 3, 4
        """
        if random.random() < (self.mu * self.phi):
            return random.randint(1, 4)
    
    def die(self):
        """
        When called, function determines if the animal will die or not
        :return: Bool, True if the animal should die, False otherwise
        """
        if self.phi == 0:
            return True 
        else:
            return random.random() < (self.omega * (1 - self.phi))


class Herbivore(Animal):
    """
    Subclass of animal. The parameters in this subclass is defined with built-in default values for
    all parameters, so the simulations can be carried out without the need to set parameters. It is
    possible to change a subset of the parameters by providing a dictionary with those parameters
    to the :meth:'Herbivore.set_params' method. Any errors detected in the parameter dictionaries
    raises a ValueError.
    """

    # These parameters are defined at class level
    w_birth = 8.0  # The mean value of herbivores' birth weight
    sigma_birth = 1.5  # The standard deviation of the herbivores' birth weight
    beta = 0.9  # Factor of weight increase when the herbivore eats
    eta = 0.05  # Factor of weight decrease when a year is over
    a_half = 40.0  # Parameter for calculating fitness
    phi_age = 0.2  # Parameter for calculating fitness
    w_half = 10.0  # Parameter for calculating fitness
    phi_weight = 0.1  # Parameter for calculating the fitness
    mu = 0.25  # Parameter to determine if a herbivore will migrate
    gamma = 0.2  # Parameter in the formula that to determine probability to give birth
    zeta = 3.5  # Parameter to determine if mother animal weighs enough to create an offspring
    xi = 1.2  # Parameter for loss of weight multiplied with the weight of the newborn herbivore
    omega = 0.4  # Parameter to determine death probability
    F = 10.0  # Amount of fodder the animal will want to eat

    default_params = {'w_birth': w_birth,
                      'sigma_birth': sigma_birth,
                      'beta': beta,
                      'eta': eta,
                      'a_half': a_half,
                      'phi_age': phi_age,
                      'w_half': w_half,
                      'phi_weight': phi_weight,
                      'mu': mu,
                      'gamma': gamma,
                      'zeta': zeta,
                      'xi': xi,
                      'omega': omega,
                      'F': F}

    @classmethod
    def get_params(cls):
        """
        Gets herbivore parameters
        :return: Dict, class parameters
        """
        return {'w_birth': cls.w_birth, 'sigma_birth': cls.sigma_birth, 'beta': cls.beta,
                'eta': cls.eta, 'a_half': cls.a_half, 'phi_age': cls.phi_age, 'w_half': cls.w_half,
                'phi_weight': cls.phi_weight, 'mu': cls.mu, 'gamma': cls.gamma, 'zeta': cls.zeta,
                'xi': cls.xi, 'omega': cls.omega, 'F': cls.F}

    @classmethod
    def set_params(cls, new_params):
        """
        Class method to set new parameters to the herbivore class
        :param new_params: dictionary with legal keys 'w_birth', 'sigma_birth', 'beta', 'eta',\
                                'a_half', 'phi_age', 'w_half', 'phi_weight'\
                                'mu', 'gamma', 'zeta', 'xi', 'F'
        :raises ValueError, KeyError
        """
        for key in new_params:
            if key not in cls.default_params:
                raise KeyError('Invalid parameter name ' + key)

        if 'w_birth' in new_params:
            if not new_params['w_birth'] >= 0:
                raise ValueError('New animals must have positive weight! w_birth must be positive')
            cls.w_birth = new_params['w_birth']

        if 'sigma_birth' in new_params:
            if not new_params['sigma_birth'] >= 0:
                raise ValueError('sigma_birth must be positive')
            cls.sigma_birth = new_params['sigma_birth']

        if 'beta' in new_params:
            if not 0 <= new_params['beta'] <= 1:
                raise ValueError('beta (factor of weight gain) must be in interval [0,1]')
            cls.beta = new_params['beta']

        if 'eta' in new_params:
            if not 0 <= new_params['eta'] <= 1:
                raise ValueError('eta (factor of weight loss) must be in interval [0,1]')
            cls.eta = new_params['eta']

        if 'a_half' in new_params:
            if not new_params['a_half'] >= 0:
                raise ValueError('a_half must be positive')
            cls.a_half = new_params['a_half']

        if 'phi_age' in new_params:
            if not new_params['phi_age'] >= 0:
                raise ValueError('phi_age must be positive')
            cls.phi_age = new_params['phi_age']

        if 'w_half' in new_params:
            if not new_params['w_half'] >= 0:
                raise ValueError('w_half must be positive')
            cls.w_half = new_params['w_half']

        if 'phi_weight' in new_params:
            if not new_params['phi_weight'] >= 0:
                raise ValueError('phi_weight must be positive')
            cls.phi_weight = new_params['phi_weight']

        if 'mu' in new_params:
            if not new_params['mu'] >= 0:
                raise ValueError('mu must be positive')
            cls.mu = new_params['mu']

        if 'gamma' in new_params:
            if not new_params['gamma'] >= 0:
                raise ValueError('gamma must be positive')
            cls.gamma = new_params['gamma']

        if 'zeta' in new_params:
            if not new_params['zeta'] > 1:
                raise ValueError('zeta must be more than one')
            cls.zeta = new_params['zeta']

        if 'xi' in new_params:
            if not new_params['xi'] >= 1:
                raise ValueError('xi must be 1 or greater')
            cls.xi = new_params['xi']

        if 'omega' in new_params:
            if not 0 <= new_params['omega'] <= 1:
                raise ValueError('omega must be between 0 or 1')
            cls.omega = new_params['omega']

        if 'F' in new_params:
            if not new_params['F'] >= 0:
                raise ValueError('F must be positive')
            cls.F = new_params['F']

    def __init__(self, age=0, weight=None):
        super().__init__(age=age, weight=weight)

    def __repr__(self):
        return "Herbivore(age={}, weight={})".format(self.a, self.w)

    def eat(self, f_available):
        """
        The only method for the carnivore class, that differs from the herbivore class. Every time a
        herbivore eats, the animals tries to consume an amount F of fodder, and it's weight will
        increase with a factor beta * F
        :param f_available: Amount of available fodder in the tile.
        :return: Fodder left after herbivore has eaten
        """
        if f_available > self.F:
            consumed = self.F
        else:
            consumed = f_available
        if consumed:
            self.w += self.beta * consumed
            self.calculate_fitness()
        return f_available - consumed


class Carnivore(Animal):
    """
    Carnivores, a subclass of animals. Living behaviour is similar to the Herbivore class except
    for the :meth:'Carnivore.eat' method, and has slightly different parameter values.
    """

    # These parameters are defined at class level
    w_birth = 6.0  # The mean value of carnivores' birth weight
    sigma_birth = 1.0  # The standard deviation of the carnivores' birth weight
    beta = 0.75  # Parameter of weight increase when the herbivore eats
    eta = 0.125  # Parameter of weight decrease when a year is over
    a_half = 60.0  # Parameter for calculating fitness
    phi_age = 0.4  # Parameter for calculating fitness
    w_half = 4.0  # Parameter for calculating fitness
    phi_weight = 0.4  # Parameter for calculating the fitness
    mu = 0.4  # Parameter to determine if a carnivore will migrate
    gamma = 0.8  # Parameter in the formula that to determine probability to give birth
    zeta = 3.5  # Parameter to determine if mother animal weighs enough to create an offspring
    xi = 1.1  # Parameter for loss of weight multiplied with the weight of the newborn herbivore
    omega = 0.9  # Parameter to determine death probability
    F = 50.0  # Amount of fodder the carnivore will want to eat
    DeltaPhiMax = 10.0  # Parameter to calculate probability of successful killing of a herbivore

    default_params = {'w_birth': w_birth,
                      'sigma_birth': sigma_birth,
                      'beta': beta,
                      'eta': eta,
                      'a_half': a_half,
                      'phi_age': phi_age,
                      'w_half': w_half,
                      'phi_weight': phi_weight,
                      'mu': mu,
                      'gamma': gamma,
                      'zeta': zeta,
                      'xi': xi,
                      'omega': omega,
                      'F': F,
                      'DeltaPhiMax': DeltaPhiMax}

    @classmethod
    def get_params(cls):
        """
        Gets herbivore parameters
        :return: Dict, class parameters
        """
        return {'w_birth': cls.w_birth, 'sigma_birth': cls.sigma_birth, 'beta': cls.beta,
                'eta': cls.eta, 'a_half': cls.a_half, 'phi_age': cls.phi_age, 'w_half': cls.w_half,
                'phi_weight': cls.phi_weight, 'mu': cls.mu, 'gamma': cls.gamma, 'zeta': cls.zeta,
                'xi': cls.xi, 'omega': cls.omega, 'F': cls.F, 'DeltaPhiMax': cls.DeltaPhiMax}

    @classmethod
    def set_params(cls, new_params):
        """
        Class method to set new parameters to the herbivore class
        :param new_params: dictionary with legal keys 'w_birth', 'sigma_birth', 'beta', 'eta',\
                                'a_half', 'phi_age', 'w_half', 'phi_weight'\
                                'mu', 'gamma', 'zeta', 'xi', 'F', 'DeltaPhiMax'
        :raises ValueError, KeyError
        """
        for key in new_params:
            if key not in cls.default_params:
                raise KeyError('Invalid parameter name ' + key)

        if 'w_birth' in new_params:
            if not new_params['w_birth'] >= 0:
                raise ValueError('New animals must have positive weight! w_birth must be >0')
            cls.w_birth = new_params['w_birth']

        if 'sigma_birth' in new_params:
            if not new_params['sigma_birth'] >= 0:
                raise ValueError('sigma_birth cannot be negative')
            cls.sigma_birth = new_params['sigma_birth']

        if 'beta' in new_params:
            if not 0 <= new_params['beta'] <= 1:
                raise ValueError('beta must be in interval [0,1]')
            cls.beta = new_params['beta']

        if 'eta' in new_params:
            if not 0 <= new_params['eta'] <= 1:
                raise ValueError('eta must be in interval [0,1]')
            cls.eta = new_params['eta']

        if 'a_half' in new_params:
            if not new_params['a_half'] >= 0:
                raise ValueError('New animals cannot have negative weight!')
            cls.a_half = new_params['a_half']

        if 'phi_age' in new_params:
            if not new_params['phi_age'] >= 0:
                raise ValueError('phi_age must be positive')
            cls.phi_age = new_params['phi_age']

        if 'w_half' in new_params:
            if not new_params['w_half'] >= 0:
                raise ValueError('w_half must be positive')
            cls.w_half = new_params['w_half']

        if 'phi_weight' in new_params:
            if not new_params['phi_weight'] >= 0:
                raise ValueError('phi_weight must be positive')
            cls.phi_weight = new_params['phi_weight']

        if 'mu' in new_params:
            if not 0 <= new_params['mu']:
                raise ValueError('mu must be non-negative')
            cls.mu = new_params['mu']

        if 'gamma' in new_params:
            if not 0 <= new_params['gamma']:
                raise ValueError('gamma')
            cls.gamma = new_params['gamma']

        if 'zeta' in new_params:
            if not new_params['zeta'] > 1:
                raise ValueError('zeta must be more than one')
            cls.zeta = new_params['zeta']

        if 'xi' in new_params:
            if not new_params['xi'] >= 1:
                raise ValueError('xi must be 1 or greater')
            cls.xi = new_params['xi']

        if 'omega' in new_params:
            if not 0 <= new_params['omega'] <= 1:
                raise ValueError('omega must be between 0 or 1')
            cls.omega = new_params['omega']

        if 'F' in new_params:
            if not new_params['F'] >= 0:
                raise ValueError('F must be non-negative')
            cls.F = new_params['F']

        if 'DeltaPhiMax' in new_params:
            if not new_params['DeltaPhiMax'] > 0:
                raise ValueError('DeltaPhiMax must be strictly positive')
            cls.DeltaPhiMax = new_params['DeltaPhiMax']

    def __init__(self, age=0, weight=None):
        super().__init__(age=age, weight=weight)

    def __repr__(self):
        return "Carnivore(age={}, weight={})".format(self.a, self.w)

    def eat(self, sorted_herbivores):
        """
        Carnivores prey on herbivores. They can prey everywhere, but they do not prey on each
        other. Carnivores try to kill herbivores in the order of fitness, i.e., the carnivore with
        the highest fitness eats first. Carnivores try to kill one herbivore at a time, beginning
        with the herbivore with lowest fitness. A carnivore will continue to kill herbivores
        until the carnivore has eaten an amount F,( i.e., eaten herbivores with a total weight ≥ F)
        or until it have tried to kill each herbivore in the cell.

        The probability of a Carnivore (c) killing a Herbivore (h) is:
        * 0 if c.phi < h.phi
        * (c.phi − h.phi) / DeltaPhiMax if 0 < (c.phi) − h.phi < DeltaPhiMax
        * 1 otherwise
        """
        hunger = self.F
        for herbivore in reversed(sorted_herbivores):
            if hunger <= 0:
                break  # Makes sure carnivore does not hunt if not hungry
            delta_phi = self.phi - herbivore.phi
            if delta_phi < 0:
                break  # Exits the for loop, as the rest of the animals will have higher fitness
            elif delta_phi < self.DeltaPhiMax:
                p_kill = delta_phi / self.DeltaPhiMax
            else:
                p_kill = 1
            if random.random() < p_kill:
                """
                If carnivore successfully kills herbivore: (Note: If this is false, then the 
                killing was unsuccessful and the carnivore will try to kill the next animal
                """
                amount_prey = herbivore.w  # Storing weight of prey
                sorted_herbivores.remove(herbivore)  # "Killing" herbivore
                if amount_prey > hunger:  # If there are more food than carnivore needs
                    self.w += self.beta * hunger  # Carnivore only eat what it want
                    self.calculate_fitness()
                    break  # and is finished hunting
                else:
                    self.w += self.beta * amount_prey
                    hunger -= amount_prey  # After eating, the animal is less hungry
                    self.calculate_fitness()
