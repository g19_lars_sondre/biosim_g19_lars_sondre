# utf-8

"""
Python package for the INF200 January Project 2019
Modelling the Ecosystem of Rossumøya
"""

__author__ = 'John Sondre Sikkeland, Lars-Petter Andersen'
__email__ = 'johnsikk@nmbu.no, larsand@nmbu.no'
