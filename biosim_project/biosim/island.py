# -*- coding: utf-8 -*-

"""
:mod:'biosim.island' provides a class island with the various
landscape types in the :mod:'bioosim.landscape' module.
Implemented landscape types:
*[O]cean
*[M]ountain
*[J]ungle
*[S]avannah
*[D]esert"
"""

__author__ = 'John Sondre Sikkeland, Lars-Petter Andersen'
__email__ = 'johnsikk@nmbu.no, larsand@nmbu.no'

from .landscape import *
import re


class Island:
    """An Island with various landscape types and animals of type Herbivore/Carnivore."""

    def __init__(self, island_map, ini_pop=None):
        """
        Constructs an island given a map. It is also possible to add an initial population to
        the island
        :param island_map: Multi-line string of the islands geography. All the lines in the
        string must be of equal length, and contain only the allowed landscape types:
        O - Ocean
        M - Mountain
        J - Jungle
        S - Savannah
        D - Desert
        The string must consist solely of “O” around the edges and no characters other than the
        ones above must occur in the string. If any of these requirements are violated,
        the method will raise a value error
        :param ini_pop: See :meth:'Island.add_population' method

        """
        self.geography = []  # Empty list where landscape tiles are stored
        if type(island_map) is not str:  # Checking that island map is given as string
            raise ValueError('Island geography must be input as multi-line string')
        if not bool(re.match('^[OJSDM\n]+$', island_map)):  # Checking input consists of valid chars
            raise ValueError("""Invalid landscape type entered. Accepted landscapes are:
                             [O]cean, [M]ountain, [J]ungle, [S]avannah, [D]esert""")
        lines = island_map.splitlines()
        if not bool(re.match('^[O]+$', lines[0])):
            # Only ocean in top row
            raise ValueError('Bad boundary. Only oceans are allowed in the top row.')
        if not bool(re.match('^[O]+$', lines[-1])):
            # Only ocean in bottom row
            raise ValueError('Bad boundary. Only oceans are allowed in the bottom row.')

        self.n_columns = len(lines[0])
        self.n_rows = len(lines)

        for line in lines:
            if len(line) != self.n_columns:  # Checking lines are of equal length
                raise ValueError('Inconsistent map. All lines must have same length')
            if not (line[0] == 'O' and line[-1] == 'O'):  # Checking Ocean start and end a line
                raise ValueError('Bad boundary. Only oceans are allowed as boundary')
            row = []  # Island map is read line by line, and landscapes are saved in a row
            for char in line:
                if char == 'O':
                    row.append(Ocean())
                elif char == 'J':
                    row.append(Jungle())
                elif char == 'S':
                    row.append(Savannah())
                elif char == 'D':
                    row.append(Desert())
                elif char == 'M':
                    row.append(Mountain())
            self.geography.append(row)

        if ini_pop:  # If an initial population is provided right away
            self.add_population(population=ini_pop)  # Call the add_population method

    def add_population(self, population):
        """
        Method to add population to the island.
        :param population: an iterable data container.
        Each item in population is a dictionary with two elements,
        'loc' (Location) and 'pop' (Population).

        'loc' is a two-element tuple which provides coordinate on the island. If the coordinates
        provided are non-existing the method raises a ValueError.

        'pop' is a list with one element per animal, and each item in 'pop' is a dictionary with
        elements 'species', 'age' and 'weight'. 'Species' element has either value 'Herbivore' og
        'Carnivore'.

        'age' shall be a non-negative integer
        'weight' shall be a positive number
        """
        for location in population:
            i, j = location['loc']
            if i <= 0 or i > self.n_rows or j <= 0 or j > self.n_columns:
                raise ValueError('Non existing coordinates provided')
            if not self.geography[i-1][j-1].habitable:
                raise ValueError('Animals can not live here: ', location['loc'])
            for animal in location['pop']:
                self.geography[i-1][j-1].add_animal(a_type=animal['species'],
                                                    age=animal['age'],
                                                    weight=animal['weight'])

    def one_year(self):
        """
        Calling this provokes an annual cycle on the island. The cycle goes as follows:
        1. Fodder grows in the savannah and jungle.
        2. Animals eat (Herbivores first, then carnivores.
        3. Animals give birth
        4. Animals migrate
        5. Animals age
        6. Animals lose weight
        7. Animals die
        """
        for row in self.geography:
            for tile in row:
                if tile.habitable:            #
                    tile.regrowth()           # step 1 in cycle
                    tile.animals_eat()        # step 2
                    tile.animals_procreate()  # step 3
                    tile.animals_migrate()    # step 4.1
        self.move_animals()                   # step 4.2
        for row in self.geography:
            for tile in row:
                if tile.habitable:
                    tile.animals_age_and_lose_weight()  # step 5 and 6
                    tile.animals_die()                  # step 7

    def move_animals(self):
        """
        This method is called after :meth:'Landscape.migrate' method is called on each tile on
        the island.
        This method loops through all the tiles on the island that might have animals in their
        respective move lists: (The method skips over all tiles with .habitable = False)
        tile.move_north
        tile.move_east
        tile.move_south
        tile.move_west
        *If the list has animals, the function will first check that the tile the animals want to
        go to is habitable.
        *If that tile is habitable the animals will be put in the new tiles respective animal list
        h_pop or c_pop
        *If the tile is not habitable, the animals will be put back to the list they came from.
        ---
        note:: Border tiles are not looped over, because they should never have animals.
        """

        for i in range(1, self.n_rows - 1):  # For all rows on island except first and last
            for j in range(1, self.n_columns - 1):  # For all tiles in row except first and last
                this = self.geography[i][j]  # The tile we are currently checking
                north = self.geography[i-1][j]      # Tile above
                east = self.geography[i][j+1]       # Tile to the right
                south = self.geography[i+1][j]      # Tile below
                west = self.geography[i][j-1]       # Tile to the left
                if this.move_north:  # If tile has animals to move north
                    if north.habitable:  # If the tile north is habitable
                        while this.move_north:
                            animal = this.move_north.pop()
                            if animal.__class__.__name__ == 'Herbivore':
                                north.h_pop.append(animal)
                            if animal.__class__.__name__ == 'Carnivore':
                                north.c_pop.append(animal)

                if this.move_east:  # If tile has animals to move east
                    if east.habitable:  # If the tile east is habitable
                        while this.move_east:
                            animal = this.move_east.pop()
                            if animal.__class__.__name__ == 'Herbivore':
                                east.h_pop.append(animal)
                            if animal.__class__.__name__ == 'Carnivore':
                                east.c_pop.append(animal)

                if this.move_south:  # If tile has animals to move south
                    if south.habitable:  # If the tile south is habitable
                        while this.move_south:
                            animal = this.move_south.pop()
                            if animal.__class__.__name__ == 'Herbivore':
                                south.h_pop.append(animal)
                            if animal.__class__.__name__ == 'Carnivore':
                                south.c_pop.append(animal)

                if this.move_west:  # If tile has animals to move west
                    if west.habitable:  # If the tile west is habitable
                        while this.move_west:
                            animal = this.move_west.pop()
                            if animal.__class__.__name__ == 'Herbivore':
                                west.h_pop.append(animal)
                            if animal.__class__.__name__ == 'Carnivore':
                                west.c_pop.append(animal)
                self.replace(tile=this)  # calls replace to send animals back to h_pop and c_pop

    @staticmethod
    def replace(tile):
        """
        Takes in a tile and replace animals if they are not moved
        :param tile: Landscape type with move lists (move_north, move_east, move_south,
        move_west)
        """
        for movelist in (tile.move_north, tile.move_east, tile.move_south, tile.move_west):
            while movelist:
                animal = movelist.pop()
                if animal.__class__.__name__ == 'Herbivore':
                    tile.h_pop.append(animal)
                elif animal.__class__.__name__ == 'Carnivore':
                    tile.c_pop.append(animal)
                else:
                    raise ValueError('Unknown animal ', animal.__class__.__name__)

    def animals_per_tile(self):
        """Makes a list with same dimensions as geography"""
        db = []
        for i in range(self.n_rows):
            for j in range(self.n_columns):
                tile = self.geography[i][j]
                db.append([i+1, j+1, tile.nh, tile.nc])
        return db

    def animal_counts(self):
        """
        Count animals on the island.

        :return: tuple, two-element tuple with counts of Herbivores and Carnivores on the island
        """
        pr_cell = self.animals_per_tile()
        num_herbivores = sum(row[2] for row in pr_cell)
        num_carnivores = sum(row[3] for row in pr_cell)
        return num_herbivores, num_carnivores
