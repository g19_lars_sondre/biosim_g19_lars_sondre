===========================
BioSim project January 2019
===========================

This is a simulation of animals on the Island Rossumøya.
The simulation implements animal types Herbivores and Carnivores.
5 different landscape types are implemented:

 - Ocean
 - Mountain
 - Jungle
 - Savannah
 - Desert

The simulation module can simulate population dynamics on the Island.

Contents
--------

- biosim: Python package for simulation of population dynamics on an island
- examples: Scripts illustrating the use of the package
- tests: Tests for the different classes


To install, run::
    python setup.py install          (installs to default location for all users)
    python setup.py install --user   (installs to default location for user)

John Sondre Sikkeland, Lars-Petter Andersen / NMBU, January 2019

