# -*- coding: utf-8 -*-
from setuptools import setup
import codecs
import os

__author__ = 'John Sondre Sikkeland, Lars-Petter Andersen'
__email__ = 'johnsikk@nmbu.no, larsand@nmbu.no'


def read_readme():
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, 'README.txt'), encoding='utf-8') as f:
        long_description = f.read()
    return long_description


setup(
      # Basic information
      name='BioSim',
      version='0.1',

      # Packages to include
      packages=['biosim'],

      # Required packages not included in Python standard library
      requires=['numpy', 'pandas', 'matplotlib', 'pytest'],

      # Metadata
      description='A population dynamics simulation',
      long_description=read_readme(),
      author='John Sondre Sikkeland, Lars-Petter Andersen, NMBU',
      author_email='johnsikk@nmbu.no, larsand@nmbu.no',
      url='https://bitbucket.org/g19_lars_sondre/biosim_g19_lars_sondre/',
      keywords='Population Dynamics Simulation',
      license='MIT License',
      classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Science :: Population Dynamics',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
        ]
)
